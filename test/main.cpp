#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#include <alogui/Window.h>
#include <alogui/Desktop.h>
#include <alogui/widgets/Label.h>
#include <alogui/widgets/Button.h>
#include <alogui/containers/BoxContainer.h>
#include <alogui/widgets/RichTextBox.h>
#include <alogui/containers/ScrollContainer.h>
#include <alogui/widgets/LineEdit.h>
#include <alogui/widgets/MessageQueue.h>
#include <alogui/widgets/Gallery.h>

void initGui()
{
	using namespace alogui;

	Window* gui = new Window();
	gui->setPosition(100, 75);
	gui->setTitle("Untitled Window");

	BoxContainer* container = new BoxContainer();

	/*Label* label1 = new Label();
	label1->setText("This is a label with text in it.");
	container->add(label1);

	Label* label2 = new Label();
	label2->setText("This is a second label,\nwith other text in it.");
	container->add(label2);

	Button* button = new Button();
	container->add(button);*/

	/*RichTextBox* tb = new RichTextBox();
	tb->setText("This is a test! I like \\c1apples \\c0and \\c2food \\c0and stuff!\nHello this is a test with a random newline.");
	container->add(tb);*/

	ScrollContainer* scroll = new ScrollContainer();
	container->add(scroll);
	//scroll->setSizePolicy(SizePolicy(SizePolicy::Preferred, SizePolicy::Preferred));

	/*RichTextBox* tb = new RichTextBox();
	tb->setText("This is a test! I like \\c1apples \\c0and \\c2food \\c0and stuff!\nHello this is a test with a random newline");
	scroll->add(tb);*/

	/*Gallery* gallery = new Gallery();
	scroll->add(gallery);

	for (int i = 0; i < 10; i++) {
		sf::Image img;
		img.loadFromFile("grass.png");
		gallery->add("grass", img);
	}*/

	/*BoxContainer* msgs = new BoxContainer();
	{
		Label* lbl = new Label();
		lbl->setText("desu 1");
		msgs->add(lbl);
	}
	{
		Label* lbl = new Label();
		lbl->setText("desu 2");
		msgs->add(lbl);
	}
	scroll->add(msgs);*/

	/*Label* label1 = new Label();
	label1->setText("This is where the typetype commandteller goes.");
	container->add(label1);*/

	gui->add(container);

	Desktop::get()->add(gui);
}

int main()
{
	sf::ContextSettings settings;
	settings.antialiasingLevel = 4;
	sf::RenderWindow window(sf::VideoMode(800, 600), "alogui", sf::Style::Default, settings);
	window.setVerticalSyncEnabled(true);

	initGui();

	sf::Clock clock;
	while (window.isOpen()) {
		sf::Event ev;
		while (window.pollEvent(ev)) {
			if (ev.type == sf::Event::Closed)
				window.close();
			else
				alogui::Desktop::get()->handleEvent(ev);
		}

		sf::Time dt = clock.restart();
		alogui::Desktop::get()->update(dt.asSeconds());

		window.clear(sf::Color(80, 82, 84 /*42, 45, 48*/));
		alogui::Desktop::get()->draw(window);
		window.display();
	}
}
#include <alogui/Widget.h>

#include <iostream>

#include <alogui/Desktop.h>  // remove on destroy, DPI scaling

#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/View.hpp>
#include <SFML/Window/Event.hpp>
#include <SFML/Graphics/RectangleShape.hpp>  // for debugDraw

namespace alogui
{

static Vector2i DEFAULT_MAX_SIZE(65536, 65536);
static Vector2i DEFAULT_MIN_SIZE((int) ceil(12 * Desktop::get()->getDPIScale()), (int) ceil(16 * Desktop::get()->getDPIScale()));

Widget::Widget()
	: mSizePolicy(SizePolicy(SizePolicy::Preferred, SizePolicy::Preferred)),
	mMaximumSize(DEFAULT_MAX_SIZE),
	mMinimumSize(DEFAULT_MIN_SIZE)
{
	mParent = nullptr;
	mResizePending = false;
	mFocusWidget = NULL;
}

Widget::~Widget()
{
	if (getFocusWidget() == this)
		clearFocus();

	// TODO - invalidate script reference
	if (mParent != nullptr)
		mParent->removeChild(this);

	Desktop::get()->remove(this);
}

bool Widget::handleEvent(const alogui::Event& ev)
{
	switch (ev.type) {
	case Event::MOUSE_DOWN:
		if (onMouseDown(ev.mouse.pos - getPosition(), ev.mouse.button)) {
			grabFocus();
			return true;
		}
	case Event::MOUSE_UP:
		return onMouseUp(ev.mouse.pos - getPosition(), ev.mouse.button);
	case Event::MOUSE_MOVED:
		return onMouseMoved(ev.mouse.pos - getPosition());
	case Event::KEY_DOWN:
		return onKeyDown(ev.key.code);
	case Event::KEY_UP:
		return onKeyUp(ev.key.code);
	case Event::TEXT_ENTERED:
		return onTextEntered(ev.text.text);
	}

	return false;
}

/*Widget* Widget::findWidgetAt(Vector2i pos)
{
	return getBox().contains(pos) ? this : NULL;
}*/

Widget* Widget::getTopLevel()
{
	Widget* widget = this;
	while (widget->mParent)
		widget = widget->mParent;

	return widget;
}

Widget* Widget::getFocusWidget()
{
	return getTopLevel()->mFocusWidget;
}

void Widget::grabFocus()
{
	Widget* top = getTopLevel();

	// are we already focused?
	if (top->mFocusWidget == this)
		return;

	// if someone else was focused, they lose focus
	if (top->mFocusWidget)
		top->mFocusWidget->onFocusLost();

	getTopLevel()->mFocusWidget = this;
	onFocusGained();
}

void Widget::clearFocus()
{
	Widget* top = getTopLevel();
	if (top->mFocusWidget)
		top->mFocusWidget->onFocusLost();

	getTopLevel()->mFocusWidget = NULL;
}

void Widget::update(float dt)
{
	if (mResizePending) {
		// if we're not top-level, we assume top level widget has already been notified and do nothing
		if (isTopLevel()) {
			Vector2i min = minimumSize();
			Vector2i max = maximumSize();
			Vector2i size = getSizeHint();
			size.x = std::min(std::max(size.x, min.x), max.x);
			size.y = std::min(std::max(size.y, min.y), max.y);
			setSize(size);
		}

		mResizePending = false;
	}

	onUpdate(dt);
}

void Widget::debugDraw(sf::RenderTarget& target)
{
	static sf::RectangleShape rect;
	rect.setFillColor(sf::Color::Transparent);
	rect.setOutlineColor(sf::Color::Red);
	rect.setOutlineThickness(0.5f);
	rect.setPosition(0, 0);
	rect.setSize(getSize());
	target.draw(rect);
}

void Widget::draw(sf::RenderTarget& target)
{
	// translate matrix by mPosition
	const sf::View orig_view = target.getView();
	sf::View view = orig_view;
	view.move(mPosition * -1);
	target.setView(view);

	// TODO add clipping

	//debugDraw(target);

	onDraw(target);

	// undo translation
	target.setView(orig_view);
}

void Widget::setPosition(Vector2i pos)
{
	mPosition = pos;
}

void Widget::setSize(Vector2i size)
{
	mSize = size;
	onSizeChanged();
}

void Widget::requestResize()
{
	if (mParent)
		getParent()->requestResize();
	else
		mResizePending = true;
}

Vector2i Widget::getGlobalPosition() const
{
	Vector2i pos = mPosition;
	const Widget* parent = mParent;
	while (parent) {
		pos += parent->getPosition();
		parent = parent->mParent;
	}
	return pos;
}

bool Widget::useHeightForWidth() const
{
	return false;
}

int Widget::heightForWidth(int w) const
{
	return 0;
}

void Widget::onSizePolicyChanged()
{
	requestResize();
}

void Widget::setSizePolicy(const SizePolicy& sp)
{
	mSizePolicy = sp;
	onSizePolicyChanged();
}

Vector2i Widget::maximumSizeHint() const
{
	return DEFAULT_MAX_SIZE;
}

Vector2i Widget::maximumSize() const
{
	Vector2i size(maximumSizeHint());

	bool needSizeHint = (getSizePolicy().x() & SizePolicy::GrowFlag) == 0 || (getSizePolicy().y() & SizePolicy::GrowFlag) == 0;
	Vector2i normalSizeHint = needSizeHint ? getSizeHint() : Vector2i();
	if ((getSizePolicy().x() & SizePolicy::GrowFlag) == 0 && size.x == DEFAULT_MAX_SIZE.x) {
		size.x = normalSizeHint.x;
	}
	if ((getSizePolicy().y() & SizePolicy::GrowFlag) == 0 && size.y == DEFAULT_MAX_SIZE.y) {
		size.y = normalSizeHint.y;
	}

	size.x = std::min(size.x, mMaximumSize.x);
	size.y = std::min(size.y, mMaximumSize.y);

	return size;
}

Vector2i Widget::minimumSizeHint() const
{
	return DEFAULT_MIN_SIZE;
}

Vector2i Widget::minimumSize() const
{
	Vector2i size(minimumSizeHint());

	bool needSizeHint = (getSizePolicy().x() & SizePolicy::ShrinkFlag) == 0 || (getSizePolicy().y() & SizePolicy::ShrinkFlag) == 0;
	Vector2i normalSizeHint = needSizeHint ? getSizeHint() : Vector2i();
	if ((getSizePolicy().x() & SizePolicy::ShrinkFlag) == 0) {
		size.x = normalSizeHint.x;
	}
	if ((getSizePolicy().y() & SizePolicy::ShrinkFlag) == 0) {
		size.y = normalSizeHint.y;
	}

	size.x = std::max(size.x, mMinimumSize.x);
	size.y = std::max(size.y, mMinimumSize.y);

	return size;
}


void Widget::setMaximumSize(Vector2i size)
{
	mMaximumSize = size;
	requestResize();
}

void Widget::setMinimumSize(Vector2i size)
{
	mMinimumSize = size;
	requestResize();
}

}

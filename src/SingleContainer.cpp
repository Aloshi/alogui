#include <alogui/SingleContainer.h>

#include <assert.h>

namespace alogui
{

Widget* SingleContainer::getChild() const
{
	return mChildren.empty() ? nullptr : mChildren.front();
}

bool SingleContainer::onAdd(Widget* child)
{
	assert(mChildren.size() == 0);
	return true;
}

}
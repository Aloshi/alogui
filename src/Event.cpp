#include <alogui/Event.h>

#include <SFML/Window/Event.hpp>

namespace alogui {

Event::EventType sfEventTypeToGuiEventType(sf::Event::EventType type)
{
	switch (type) {
	case sf::Event::MouseButtonPressed: return alogui::Event::MOUSE_DOWN;
	case sf::Event::MouseButtonReleased: return alogui::Event::MOUSE_UP;
	case sf::Event::MouseMoved: return alogui::Event::MOUSE_MOVED;
	case sf::Event::KeyPressed: return alogui::Event::KEY_DOWN;
	case sf::Event::KeyReleased: return alogui::Event::KEY_UP;
	case sf::Event::TextEntered: return alogui::Event::TEXT_ENTERED;
	default: return alogui::Event::UNKNOWN;
	}
}

Event Event::fromSFMLEvent(const sf::Event& ev)
{
	std::string text;
	if (ev.type == sf::Event::TextEntered) {
		text = (char*) sf::String(ev.text.unicode).toUtf8().c_str();
	}

	Event guiEvent = {
		{ Vector2i(0, 0), ev.mouseButton.button },
		{ ev.key.code },
		{ text },
		sfEventTypeToGuiEventType(ev.type)
	};

	guiEvent.mouse.pos = (ev.type == sf::Event::MouseMoved
		? Vector2i(ev.mouseMove.x, ev.mouseMove.y)
		: Vector2i(ev.mouseButton.x, ev.mouseButton.y));

	return guiEvent;
}

}
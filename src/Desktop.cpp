#include <alogui/Desktop.h>
#include <alogui/Widget.h>

#include <SFML/Window/Event.hpp>

#include <assert.h>

namespace alogui
{

Desktop* Desktop::gInstance = NULL;

Desktop::Desktop()
  : mDPIScale(1.0f)
{
}

Desktop::~Desktop()
{
	while (mWidgets.size()) {
		remove(mWidgets.front());
	}
}

Desktop* Desktop::get()
{
	if (!gInstance)
		gInstance = new Desktop();

	return gInstance;
}

void Desktop::add(Widget* widget)
{
	auto it = std::find(mWidgets.begin(), mWidgets.end(), widget);
	if (it == mWidgets.end()) {
		mWidgets.push_back(widget);
		widget->onWake();
	}
}

void Desktop::remove(Widget* widget)
{
	auto it = std::find(mWidgets.begin(), mWidgets.end(), widget);
	if (it != mWidgets.end()) {
		widget->onSleep();
		mWidgets.erase(it);
	}
}

bool Desktop::added(Widget* widget)
{
	return (std::find(mWidgets.begin(), mWidgets.end(), widget) != mWidgets.end());
}

bool Desktop::handleEvent(const sf::Event& ev)
{
	if (mWidgets.empty())
		return false;

	const Event guiEvent(Event::fromSFMLEvent(ev));

	switch (guiEvent.type)
	{
	case Event::MOUSE_DOWN:
		for (Widget* widget : mWidgets) {
			if (widget->getBox().contains(guiEvent.mouse.pos)) {
				makeActive(widget);
				return widget->handleEvent(guiEvent);
			}
		}
		break;

	case Event::MOUSE_UP:
	case Event::MOUSE_MOVED:
	case Event::KEY_DOWN:
	case Event::KEY_UP:
	case Event::TEXT_ENTERED:
		return mWidgets.front()->handleEvent(guiEvent);
		break;
	}

	return false;
}

void Desktop::makeActive(Widget* widget)
{
	if (mWidgets.front() == widget)
		return;

	auto it = std::find(mWidgets.begin(), mWidgets.end(), widget);
	assert(it != mWidgets.end());
	mWidgets.erase(it);

	mWidgets.front()->clearFocus();

	mWidgets.insert(mWidgets.begin(), widget);
}

void Desktop::update(float dt)
{
	for (Widget* widget : mWidgets) {
		widget->update(dt);
	}
}

void Desktop::draw(sf::RenderTarget& target)
{
	// draw back to front
	for (auto it = mWidgets.rbegin(); it != mWidgets.rend(); it++) {
		(*it)->draw(target);
	}
}

}

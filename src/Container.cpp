#include <alogui/Container.h>

#include <assert.h>
#include <iostream>

namespace alogui
{

Container::~Container()
{
	// since child destructors can affect mChildren...
	while (mChildren.size()) {
		Widget* child = mChildren.back();
		mChildren.pop_back();
		delete child;
	}
}

void Container::add(Widget* child)
{
	assert(child->getParent() == nullptr);
	mChildren.push_back(child);
	child->mParent = this;
	requestResize();
}

void Container::remove(Widget* child)
{
	if (child->getParent() == nullptr)
		return;

	assert(child->getParent() == this);
	auto it = std::find(mChildren.begin(), mChildren.end(), child);
	if (it != mChildren.end()) {
		mChildren.erase(it);
		requestResize();
	}
}

void Container::onUpdate(float dt)
{
	for (auto child : mChildren) {
		child->update(dt);
	}
}

void Container::onDraw(sf::RenderTarget& target)
{
	for (auto child : mChildren) {
		child->draw(target);
	}
}

Widget* Container::findChildAt(Vector2i pos)
{
	const Vector2i localPos = pos;

	for (Widget* child : mChildren) {
		if (child->getBox().contains(localPos))
			return child;
	}

	return NULL;
}

bool Container::handleEvent(const Event& ev)
{
	Event childEvent = ev;

	// fix coordinates
	if (ev.type == Event::MOUSE_DOWN || ev.type == Event::MOUSE_UP || ev.type == Event::MOUSE_MOVED)
		childEvent.mouse.pos -= getPosition();

	switch (ev.type) {
	case Event::MOUSE_DOWN:
	{
		Widget* child = findChildAt(childEvent.mouse.pos);
		if (child && child != this && child->handleEvent(childEvent)) {
			return true;
		}
		break;
	}

	case Event::MOUSE_UP:
	case Event::MOUSE_MOVED:
	case Event::KEY_UP:
	case Event::KEY_DOWN:
	case Event::TEXT_ENTERED:
	{
		Widget* focus = getFocusWidget();
		if (focus != NULL && focus != this) {
			// fix coordinates
			if (ev.type == Event::MOUSE_UP || ev.type == Event::MOUSE_MOVED)
				childEvent.mouse.pos -= (focus->getGlobalPosition() - getGlobalPosition() - focus->getPosition());

			if (focus->handleEvent(childEvent))
				return true;
		}
		break;
	}
	}

	return Widget::handleEvent(ev);
}

}
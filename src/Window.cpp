#include <alogui/Window.h>
#include <alogui/Desktop.h>  // for default onClose, DPI scaling

#include <assert.h>

#include <SFML/Graphics.hpp>

#include <iostream>

namespace alogui
{

#define TITLE_LEFT_PADDING ((int) ceil(3 * Desktop::get()->getDPIScale()))
#define CONTENT_PADDING ((int) ceil(6 * Desktop::get()->getDPIScale()))
#define RESIZE_HANDLE_SIZE ((int) ceil(5 * Desktop::get()->getDPIScale()))
#define BUTTON_SIZE ((int) ceil(12 * Desktop::get()->getDPIScale()))
#define BUTTON_SPACING ((int) ceil(5 * Desktop::get()->getDPIScale()))
#define BUTTON_TRAY_SIZE (mCanClose ? (BUTTON_SIZE + BUTTON_SPACING*2) : 0)

#define CLOSE_BUTTON_NORMAL sf::Color(231, 76, 76)
#define CLOSE_BUTTON_HIGHLIGHT sf::Color(231, 100, 100)
#define CLOSE_BUTTON_CLICK sf::Color(255, 40, 40)

Window::Window() : mDragging(false), mResizing(false), mResizeDiff(0, 0), mCanClose(true), mClosing(false)
{
  float dpiScale = Desktop::get()->getDPIScale();

	//mBackground.setFillColor(sf::Color(28, 30, 34, 232));
	mBackground.setFillColor(sf::Color(40, 44, 51, 232));
	mBackground.setCornerRadius(2 * dpiScale, 2 * dpiScale, 3 * dpiScale, 3 * dpiScale);
	mBackground.setCornerPointCount(3 * dpiScale);

	mTitlebarBackground.setFillColor(sf::Color(23, 25, 28, 250));
	mTitlebarBackground.setCornerRadius(2 * dpiScale, 2 * dpiScale, 0, 0);
	mTitlebarBackground.setCornerPointCount(3 * dpiScale);

	mTitlebarLabel.setAlignment(Label::ALIGN_LEFT);

	mCloseButton.setRadius(5 * dpiScale);
	mCloseButton.setFillColor(CLOSE_BUTTON_NORMAL);

	mResizeHandle.setPointCount(3 * dpiScale);
	mResizeHandle.setPoint(0, sf::Vector2f(0, 0));
	mResizeHandle.setPoint(1, sf::Vector2f(RESIZE_HANDLE_SIZE, 0));
	mResizeHandle.setPoint(2, sf::Vector2f(0, RESIZE_HANDLE_SIZE));
	mResizeHandle.setFillColor(sf::Color(128, 128, 128));
}

void Window::onSizeChanged()
{
	// update drawables
	mBackground.setSize(getSize());

	// titlebar
	mTitlebarBackground.setSize(sf::Vector2f((float)width(), (float)std::min(titlebarSizeHint().y, height())));
	mTitlebarLabel.setPosition(TITLE_LEFT_PADDING, 0);
	mTitlebarLabel.setSize(Vector2i(width() - BUTTON_TRAY_SIZE, std::min(mTitlebarLabel.getSizeHint().y, height())));
	mCloseButton.setPosition((float)(width() - BUTTON_TRAY_SIZE + BUTTON_SPACING), (mTitlebarBackground.getSize().y - mCloseButton.getRadius()*2) / 2.0f);

	// move resize corner
	mResizeHandle.setPosition((float)(width() - RESIZE_HANDLE_SIZE - 1), (float)(height() - RESIZE_HANDLE_SIZE - 1));

	// update content pos/size
	auto tbb = titlebarBox();
	if (getChild()) {
		const Vector2i contentPos(CONTENT_PADDING, tbb.bottom() + CONTENT_PADDING);
		getChild()->setPosition(contentPos);

		const Vector2i contentSize(width() - CONTENT_PADDING * 2, height() - CONTENT_PADDING * 2 - tbb.bottom());
		getChild()->setSize(contentSize);
	}
}

void Window::setCanClose(bool canClose)
{
	mCanClose = canClose;
	requestResize();
}

Vector2i Window::getSizeHint() const
{
	const Vector2i contentSize = getChild() ? getChild()->getSizeHint() : Vector2i(0, 0);
	const Vector2i titleSize = titlebarSizeHint();
	const Vector2i size(std::max(titleSize.x, contentSize.x) + CONTENT_PADDING * 2, titleSize.y + contentSize.y + 2 * CONTENT_PADDING);
	return size;
}

Vector2i Window::minimumSizeHint() const
{
	const Vector2i contentSize = getChild() ? getChild()->minimumSize() : Vector2i(0, 0);
	const Vector2i titleSize = titlebarSizeHint();
	const Vector2i size(std::max(titleSize.x, contentSize.x) + CONTENT_PADDING * 2, titleSize.y + contentSize.y + 2 * CONTENT_PADDING);
	return size;
}

Vector2i Window::maximumSizeHint() const
{
	const Vector2i contentSize = getChild() ? getChild()->maximumSize() : Vector2i(0, 0);
	const Vector2i titleSize = titlebarSizeHint();
	const Vector2i size(std::max(titleSize.x, contentSize.x) + CONTENT_PADDING * 2, titleSize.y + contentSize.y + 2 * CONTENT_PADDING);
	return size;
}

Box2i Window::titlebarBox() const
{
	return Box2i(0, 0, width(), mTitlebarLabel.getSize().y);
}

// TODO rename this to getTitlebarSizeHint
Vector2i Window::titlebarSizeHint() const
{
	return mTitlebarLabel.getSizeHint() + Vector2i(TITLE_LEFT_PADDING + BUTTON_TRAY_SIZE, 0);
}

void Window::setTitle(const std::string& title)
{
	mTitlebarLabel.setText(title);
	requestResize();
}

void Window::onUpdate(float dt)
{
	if (mResizeDiff.x != 0 || mResizeDiff.y != 0) {
		setSize(getSize() + mResizeDiff);
		mResizeDiff = Vector2i(0, 0);
	}
}

void Window::onDraw(sf::RenderTarget& target)
{
	target.draw(mBackground);
	target.draw(mTitlebarBackground);
	mTitlebarLabel.draw(target);

	if (mCanClose)
		target.draw(mCloseButton);

	target.draw(mResizeHandle);

	if (getChild())
		getChild()->draw(target);
}

bool Window::onMouseDown(Vector2i pos, int button)
{
	if (button == 0) {
		if (mCloseButton.getGlobalBounds().contains(pos) && mCanClose) {
			mClosing = true;
			mCloseButton.setFillColor(CLOSE_BUTTON_CLICK);
			return true;
		} else if (titlebarBox().contains(pos)) {
			mDragging = true;
			mLastMousePos = pos;
			return true;
		} else if (mResizeHandle.getGlobalBounds().contains(pos)) {
			mResizing = true;
			mLastMousePos = pos;
			return true;
		}
	}

	return false;
}

bool Window::onMouseMoved(Vector2i pos)
{
	if (mDragging) {
		setPosition(getPosition() + (pos - mLastMousePos));
		//mLastMousePos = pos;
		return true;
	}
	if (mResizing) {
		Vector2i diff = (pos - mLastMousePos);
		Vector2i next = getSize() + diff + mResizeDiff;
		Vector2i max = maximumSize();
		Vector2i min = minimumSize();

		if (next.x < min.x && getSize().x > min.x) {
			// force to minimum
			int extra = next.x - min.x;
			next.x -= extra;
			diff.x -= extra;
			pos.x -= extra;
		}
		if (next.y < min.y && getSize().y > min.y) {
			// force to minimum
			int extra = next.y - min.y;
			next.y -= extra;
			diff.y -= extra;
			pos.y -= extra;
		}

		// same as above for max
		if (next.x > max.x && getSize().x < max.x) {
			// force to maximum
			int extra = next.x - max.x;
			next.x -= extra;
			diff.x -= extra;
			pos.x -= extra;
		}
		if (next.y > max.y && getSize().y < max.y) {
			// force to maximum
			int extra = next.y - max.y;
			next.y -= extra;
			diff.y -= extra;
			pos.y -= extra;
		}


		if (next.x <= max.x && next.x >= min.x) {
			mResizeDiff.x += diff.x;
			mLastMousePos.x = pos.x;
		}

		if(next.y >= min.y && next.y <= max.y)
		{
			mResizeDiff.y += diff.y;
			mLastMousePos.y = pos.y;
		}
		return true;
	}
	if (mCanClose) {
		bool highlighted = mCloseButton.getGlobalBounds().contains(pos);
		if (mClosing)
			mCloseButton.setFillColor(highlighted ? CLOSE_BUTTON_CLICK : CLOSE_BUTTON_NORMAL);
		else
			mCloseButton.setFillColor(highlighted ? CLOSE_BUTTON_HIGHLIGHT : CLOSE_BUTTON_NORMAL);
	}

	return false;
}

bool Window::onMouseUp(Vector2i pos, int button)
{
	if ((mDragging || mResizing) && button == 0) {
		mDragging = false;
		mResizing = false;
		return true;
	}
	if (mClosing) {
		mClosing = false;
		mCloseButton.setFillColor(CLOSE_BUTTON_NORMAL);
		if (mCloseButton.getGlobalBounds().contains(pos)) {
			onClose();
		}
		return true;
	}

	return false;
}

void Window::onClose()
{
	alogui::Desktop::get()->remove(this);
}

}

#include <alogui/drawables/RoundedRect.h>

#include <cmath>

namespace alogui
{

RoundedRectShape::RoundedRectShape(float radius, unsigned int cornerPointCount)
	: mSize(0, 0), mRadius{ radius, radius, radius, radius }, mCornerPointCount(cornerPointCount)
{
	update();
}

void RoundedRectShape::setSize(const sf::Vector2f& size)
{
	mSize = size;
	update();
}

void RoundedRectShape::setCornerRadius(float a, float b, float c, float d)
{
	mRadius[0] = a;
	mRadius[1] = b;
	mRadius[2] = c;
	mRadius[3] = d;
	update();
}

void RoundedRectShape::setCornerRadius(float radius)
{
	for (int i = 0; i < 4; i++)
		mRadius[i] = radius;

	update();
}

void RoundedRectShape::setCornerPointCount(unsigned int count)
{
	mCornerPointCount = count;
	update();
}

std::size_t RoundedRectShape::getPointCount() const
{
	return mCornerPointCount * 4;
}

sf::Vector2f RoundedRectShape::getPoint(std::size_t index) const
{
	if (index >= mCornerPointCount * 4)
		return sf::Vector2f(0, 0);

	float deltaAngle = 90.0f / (mCornerPointCount - 1);
	sf::Vector2f center;
	unsigned int centerIndex = index / mCornerPointCount;
	unsigned int offset = 0;
	static const float pi = 3.141592654f;
	const float radius = mRadius[centerIndex];

	switch (centerIndex)
	{
	case 0: center.x = mSize.x - radius; center.y = radius; break;
	case 1: center.x = radius; center.y = radius; break;
	case 2: center.x = radius; center.y = mSize.y - radius; break;
	case 3: center.x = mSize.x - radius; center.y = mSize.y - radius; break;
	}

	return sf::Vector2f(radius*cos(deltaAngle*(index - centerIndex)*pi / 180) + center.x,
		-radius*sin(deltaAngle*(index - centerIndex)*pi / 180) + center.y);
}

}
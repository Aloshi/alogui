#include <alogui/containers/BoxContainer.h>
#include <alogui/Desktop.h>  // for DPI scaling

#include <algorithm>

#include <iostream>
#include <assert.h>

namespace alogui
{

BoxContainer::BoxContainer()
	: mOrientation(VERTICAL), mPadding((int) (6 * Desktop::get()->getDPIScale()))
{
}

static inline int addAndStickOverflow(int a, int b) {
	int64_t res = static_cast<int64_t>(a) + static_cast<int64_t>(b);
	if (res > static_cast<int64_t>(std::numeric_limits<int>::max()))
		return std::numeric_limits<int>::max();
	return static_cast<int>(res);
}

Vector2i BoxContainer::getSizeHint() const
{
	// also need to bound getSizeHint() by min/max size hints, so this is its own thing
	Vector2i size;
	for (Widget* child : mChildren) {
		Vector2i childSize = child->getSizeHint();
		const Vector2i min = child->minimumSize();
		const Vector2i max = child->maximumSize();
		childSize.x = std::max(std::min(childSize.x, max.x), min.x);
		childSize.y = std::max(std::min(childSize.y, max.y), min.y);

		// TODO this needs to handle heightForWidth
		if (mOrientation == VERTICAL) {
			size.x = std::max(size.x, childSize.x);
			size.y = addAndStickOverflow(size.y, childSize.y);
		} else {  // mOrientation == HORIZONTAL
			size.x = addAndStickOverflow(size.x, childSize.x);
			size.y = std::max(size.y, childSize.y);
		}
	}

	// add padding
	if (!mChildren.empty()) {
		if (mOrientation == VERTICAL) {
			size.y = addAndStickOverflow(size.y, getPadding() * (mChildren.size() - 1));
		} else {
			size.x = addAndStickOverflow(size.x, getPadding() * (mChildren.size() - 1));
		}
	}

	// respect user-set min/max size
	size.x = std::min(std::max(mMinimumSize.x, size.x), mMaximumSize.x);
	size.y = std::min(std::max(mMinimumSize.y, size.y), mMaximumSize.y);

	return size;
}

Vector2i BoxContainer::computeSize(size_method_t method) const
{
	Vector2i size;
	for (Widget* child : mChildren) {
		Vector2i childSize = (*child.*method)();

		// TODO this needs to handle heightForWidth
		if (mOrientation == VERTICAL) {
			size.x = std::max(size.x, childSize.x);
			size.y = addAndStickOverflow(size.y, childSize.y);
		} else {  // mOrientation == HORIZONTAL
			size.x = addAndStickOverflow(size.x, childSize.x);
			size.y = std::max(size.y, childSize.y);
		}
	}

	// add padding
	if (!mChildren.empty()) {
		if (mOrientation == VERTICAL) {
			size.y = addAndStickOverflow(size.y, getPadding() * (mChildren.size() - 1));
		} else {
			size.x = addAndStickOverflow(size.x, getPadding() * (mChildren.size() - 1));
		}
	}

	return size;
}

Vector2i BoxContainer::minimumSizeHint() const
{
	return computeSize(&Widget::minimumSize);
}

Vector2i BoxContainer::maximumSizeHint() const
{
	return computeSize(&Widget::maximumSize);
}

void BoxContainer::setOrientation(Orientation orientation)
{
	mOrientation = orientation;
	requestResize();
}

std::vector<BoxContainer::BoxItem> BoxContainer::calcItems() const
{
	std::vector<BoxItem> items(mChildren.size());
	for (unsigned int i = 0; i < mChildren.size(); i++)
	{
		Widget* child = mChildren.at(i);
		SizePolicy sp = child->getSizePolicy();
		int sizeFlags = (mOrientation == VERTICAL ? sp.y() : sp.x());

		Vector2i sizeHint;
		Vector2i minSize = child->minimumSize();
		Vector2i maxSize = child->maximumSize();
		if (mOrientation == VERTICAL && child->useHeightForWidth())
		{
			sizeHint.x = getSize().x;
			sizeHint.y = child->heightForWidth(sizeHint.x);
		} else {
			// if we ever add widthForHeight, use it here
			sizeHint = child->getSizeHint();
		}
		
		// clamp size hint by actual max/min size
		sizeHint.x = std::max(std::min(sizeHint.x, maxSize.x), minSize.x);
		sizeHint.y = std::max(std::min(sizeHint.y, maxSize.y), minSize.y);

		// calc item
		items[i].expand = (sizeFlags & SizePolicy::ExpandFlag) != 0;
		if (mOrientation == VERTICAL)
		{
			items[i].sizeHint = sizeHint.y;
			items[i].stretch = sp.stretch().y;

			// should be: if (ShrinkFlag) minSizeHint; else max(minSizeHint, sizeHint);
			items[i].minSize = sizeFlags & SizePolicy::ShrinkFlag ? minSize.y : items[i].sizeHint;
			items[i].maxSize = sizeFlags & SizePolicy::GrowFlag ? maxSize.y : items[i].sizeHint;
		} else {
			items[i].sizeHint = sizeHint.x;
			items[i].stretch = sp.stretch().x;

			items[i].minSize = sizeFlags & SizePolicy::ShrinkFlag ? minSize.x : items[i].sizeHint;
			items[i].maxSize = sizeFlags & SizePolicy::GrowFlag ? maxSize.x : items[i].sizeHint;
		}
	}
	return items;
}

static inline int64_t toFixed(int n) {
	return (int64_t)n * 256;
}

static inline int fwRound(int64_t i) {
	return static_cast<int>((i % 256 < 128) ? i / 256 : 1 + i / 256);
}

void BoxContainer::onSizeChanged()
{
	auto items = calcItems();

	int remainingSpace = mOrientation == VERTICAL ? getSize().y : getSize().x;
	remainingSpace -= mPadding * (mChildren.size() - 1);

	int minimums = 0;
	int preferreds = 0;
	int totalStretch = 0;
	int nExpanders = 0;
	for (unsigned int i = 0; i < mChildren.size(); i++)
	{
		minimums += items[i].minSize;
		preferreds += items[i].sizeHint;
		if (items[i].expand) {
			totalStretch += items[i].stretch;
			nExpanders++;
		}
	}

	// distribute
	// https://github.com/qt/qtbase/blob/dev/src/widgets/kernel/qlayoutengine.cpp
	std::vector<int> sizes(mChildren.size());
	std::vector<bool> done(mChildren.size(), false);
	if (preferreds <= remainingSpace)
	{
		// algorithm shamelessly stolen from Qt
		// https://github.com/qt/qtbase/blob/dev/src/widgets/kernel/qlayoutengine.cpp#L76-L345

		// 1. give fixed widgets their size hint, mark them as done
		// 2. set surplus and deficit to 0 and
		//    give every expanding widget their "fair share."
		//    if given size > max size, add leftover to "surplus"
		//    if given size < min size, add leftover to "deficit"
		// 3. if deficit > 0 and surplus >= deficit, we can afford to give space to the below-min-size expanding widgets
		//    we look for widgets where size < min size, give them their min size and mark them as done
		// 4. if surplus > 0 and surplus >= deficit, then we have some beyond-max-size widgets
		//    we look for widgets where size > max size, give them their max size and mark them as done
		// 5. go to 2 until either every widget is done or surplus equals deficit
		//    WHY UNTIL SURPLUS == DEFICIT?
		//    0 == 0, everyone is happy
		//    42 == 42, a total of 42 will get shrunk in the second loop and 42 will get embiggened, next loop will be 0 == 0, so quit early

		// give non-expanding widgets their size hint, count number of expanders
		int n_not_done = mChildren.size();
		for (unsigned int i = 0; i < mChildren.size(); i++)
		{
			if (!items[i].expand) {
				sizes[i] = items[i].sizeHint;
				remainingSpace -= items[i].sizeHint;
				done[i] = true;
				n_not_done--;
			}
		}

		// set expander sizes - ignores size hint
		int surplus, deficit;
		do {
			surplus = deficit = 0;
			int64_t fwRemaining = toFixed(remainingSpace);
			int64_t fp_w = 0;
			for (unsigned int i = 0; i < mChildren.size(); i++)
			{
				// skip non-expanders and expanders that are already at their min/max size
				if (done[i] == true)
					continue;

				// this is this widget's "fair share" of the remaining space based on stretch factor
				fp_w += fwRemaining * (items[i].stretch / totalStretch);
				sizes[i] = fwRound(fp_w);
				fp_w -= toFixed(sizes[i]);  // give the remainder to the next widget

				// notice we don't mark as "done"!

				// is this size too big/too small?
				// if so, add the difference to surplus/deficit
				if (sizes[i] < items[i].minSize) {
					deficit += items[i].minSize - sizes[i];
				} else if (sizes[i] > items[i].maxSize) {
					surplus += sizes[i] - items[i].maxSize;
				}
			}

			const bool setMinSizes = deficit > 0 && surplus <= deficit;
			const bool setMaxSizes = surplus > 0 && surplus >= deficit;
			for (unsigned int i = 0; i < mChildren.size(); i++) {
				if (done[i] == true)
					continue;

				if (setMinSizes && sizes[i] < items[i].minSize) {
					sizes[i] = items[i].minSize;

					// bookkeeping
					done[i] = true;
					remainingSpace -= sizes[i];
					totalStretch -= items[i].stretch;
					n_not_done--;
				}
				if (setMaxSizes && sizes[i] > items[i].maxSize) {
					sizes[i] = items[i].maxSize;

					// bookkeeping
					done[i] = true;
					remainingSpace -= sizes[i];
					totalStretch -= items[i].stretch;
					n_not_done--;
				}
			}
		} while (n_not_done > 0 && surplus != deficit);

	} else if (minimums <= remainingSpace)
	{
		// we can fit everyone in, but not at their sizeHint sizes
		// we give everyone their minimum and try and distribute the rest fairly

		int overdraft = preferreds - remainingSpace;

		// give fixed/non-shrinking widgets their size hint, count number we can change
		int n_not_done = mChildren.size();
		for (unsigned int i = 0; i < mChildren.size(); i++)
		{
			if (items[i].minSize >= items[i].sizeHint) {
				sizes[i] = items[i].sizeHint;
				remainingSpace -= items[i].sizeHint;  // useless
				done[i] = true;
				n_not_done--;
			}
		}

		bool finished = n_not_done <= 0;
		while (!finished) {
			finished = true;
			int64_t fwRemaining = toFixed(overdraft);
			int64_t fp_w = 0;

			for (unsigned int i = 0; i < mChildren.size(); i++)
			{
				if (done[i])
					continue;

				fp_w += fwRemaining / n_not_done;
				int w = fwRound(fp_w);
				sizes[i] = items[i].sizeHint - w;
				fp_w -= toFixed(w);  // give remainder to other widgets

				if (sizes[i] < items[i].minSize) {
					done[i] = true;
					sizes[i] = items[i].minSize;
					finished = false;
					overdraft -= items[i].sizeHint - items[i].minSize;
					n_not_done--;
					break;
				}
			}
		}
	} else {
		// fuck, not enough space
		assert(false);  // TODO
	}

	Vector2i pos;
	for (unsigned int i = 0; i < mChildren.size(); i++)
	{
		Widget* child = mChildren[i];
		Vector2i size;
		if (mOrientation == VERTICAL) {
			size.x = width();
			size.y = sizes[i];
		} else {
			size.x = sizes[i];
			size.y = height();
		}

		child->setPosition(pos);
		child->setSize(size);

		if (mOrientation == VERTICAL) {
			pos += Vector2i(0, size.y + mPadding);
		} else {
			pos += Vector2i(size.x + mPadding, 0);
		}
	}
}

}

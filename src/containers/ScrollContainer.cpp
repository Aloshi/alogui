#include <alogui/containers/ScrollContainer.h>

#include <SFML/Graphics/RenderTarget.hpp>
#include <alogui/Desktop.h>  // for DPI scale

#include <algorithm>

#include <iostream>

namespace alogui
{

ScrollBar::ScrollBar(int windowSize, int contentSize, int trackSize)
{
  mWindowPosition = 0;
  mWindowSize = windowSize;
  mContentSize = contentSize;
  mTrackSize = trackSize;

  float windowContentRatio = 1.0f;
  if (contentSize != 0)
    windowContentRatio = static_cast<float>(windowSize) / contentSize;
  
  mGripSize = static_cast<int>(mTrackSize * windowContentRatio);

  // minimum grip size
  int minGripSize = (int) ceil(6 * Desktop::get()->getDPIScale());
  if (mGripSize < minGripSize)  // TODO increase this once I make Y longer than X...
    mGripSize = minGripSize;

  // maximum grip size
  if (mGripSize > mTrackSize)
    mGripSize = mTrackSize;
}

// ----------------------------

ScrollContainer::ScrollContainer()
	: mPadding(0), mCanScrollX(true), mCanScrollY(true), mDraggingX(false), mDraggingY(false)
{
	mScrollBarBackX.setFillColor(backColor());
	mScrollBarBackY.setFillColor(backColor());
	updateScrollBarColor();

	setSizePolicy(SizePolicy(SizePolicy::Expanding, SizePolicy::Expanding));
}

void ScrollContainer::requestResize()
{
	/*Widget* child = getChild();
	if (child != NULL)
	{
		//child->setSize(child->getSizeHint());
		onSizeChanged();  // update size hint, update scroll bar sizes
	}*/

	//Container::requestResize();
	onSizeChanged();
}

bool ScrollContainer::useHeightForWidth() const
{
	return getChild() ? getChild()->useHeightForWidth() : false;
}

int ScrollContainer::heightForWidth(int width) const
{
	Widget* child = getChild();
	if (child != NULL)
	{
		int height = child->heightForWidth(width - getPadding() * 2 - scrollBarWidth() - 2);
		if (mCanScrollX)
			height += scrollBarWidth() + 2;
		return height;
	}

	return 0;
}

Vector2i ScrollContainer::getSizeHint() const
{
	Widget* child = getChild();
	if (child != NULL)
	{
		Vector2i size = child->getSizeHint();
		size += Vector2i(mCanScrollY ? scrollBarWidth() + 2 : 0, mCanScrollX ? scrollBarWidth() + 2 : 0);
		return size;
	}

	return Vector2i(0, 0);
}

void ScrollContainer::onSizeChanged()
{
	Widget* child = getChild();
	Vector2i contentSize;
	if (child != NULL)
	{
		child->setPosition(getPadding(), getPadding());

		if (child->useHeightForWidth()) {
			int width = std::max(getSize().x - getPadding() * 2 - scrollBarWidth() - 2, child->minimumSize().x);
			contentSize = Vector2i(width, child->heightForWidth(width));
		} else {
			contentSize = child->getSizeHint();
		}

		child->setSize(contentSize);
	}

	Vector2i windowSize(width() - scrollBarWidth(), height() - scrollBarWidth());

	int prevDistFromBotY = mScrollBarY.contentSize() - (mScrollBarY.windowPosition() + mScrollBarY.windowSize());

	Vector2i prevWindowPositions(mScrollBarX.windowPosition(), mScrollBarY.windowPosition());
	mScrollBarX = ScrollBar(windowSize.x, contentSize.x, windowSize.x - scrollBarWidth());
	mScrollBarY = ScrollBar(windowSize.y, contentSize.y, windowSize.y - scrollBarWidth());

	if (prevDistFromBotY <= 1) {
		// we were previously fully scrolled, let's be scrolled again
		mScrollBarY.setWindowPosition(mScrollBarY.contentSize() - mScrollBarY.windowSize());
	} else {
		mScrollBarY.setWindowPosition(prevWindowPositions.y);
	}

	mScrollBarX.setWindowPosition(prevWindowPositions.x);

	updateScrollBarBack();
	updateScrollBarPos();
}

void ScrollContainer::updateScrollBarBack()
{
	mScrollBarBackY.setPosition(static_cast<float>(width() - scrollBarWidth()), 0.0f);
	mScrollBarBackY.setSize(sf::Vector2f((float)scrollBarWidth(), (float)mScrollBarY.trackSize()));

	mScrollBarBackX.setPosition(0.0f, static_cast<float>(height() - scrollBarWidth()));
	mScrollBarBackX.setSize(sf::Vector2f((float)mScrollBarX.trackSize(), (float)scrollBarWidth()));

	// also set scrollbar size and place them on their track
	mScrollBarGripX.setSize(sf::Vector2f((float)mScrollBarX.gripSize(), (float)scrollBarWidth()));
	mScrollBarGripX.setPosition(0.0f, (float)mScrollBarBackX.getPosition().y);

	mScrollBarGripY.setSize(sf::Vector2f((float)scrollBarWidth(), (float)mScrollBarY.gripSize()));
	mScrollBarGripY.setPosition((float)mScrollBarBackY.getPosition().x, 0);
}

void ScrollContainer::updateScrollBarPos()
{
	mScrollBarGripX.setPosition((float)mScrollBarX.gripPosition(), (float)mScrollBarGripX.getPosition().y);
	mScrollBarGripY.setPosition((float)mScrollBarGripY.getPosition().x, (float)mScrollBarY.gripPosition());
}

void ScrollContainer::updateScrollBarColor()
{
	mScrollBarGripX.setFillColor(mDraggingX ? draggingColor() : neutralColor());
	mScrollBarGripY.setFillColor(mDraggingY ? draggingColor() : neutralColor());

	/*if (!mDraggingX && Box2i::sf2alo(mScrollBarX.getGlobalBounds()).contains(mLastMousePos))
	{
		mScrollBarX.setFillColor(highlightColor());
	}*/
}

void ScrollContainer::onDraw(sf::RenderTarget& target)
{
	const sf::View orig_view = target.getView();
	
	// this mess is for clipping (which won't stack!)
	// there's a rounding error in here somewhere too
	Vector2i globalPos = getGlobalPosition();
	sf::View view;
	view.reset(sf::FloatRect((float)globalPos.x, (float)globalPos.y, (float)width(), (float)height()));

	view.setViewport(sf::FloatRect(globalPos.x / (float)target.getSize().x,
		globalPos.y / (float)target.getSize().y,
		width() / (float)target.getSize().x,
		height() / (float)target.getSize().y));

	view.move((float)mScrollBarX.windowPosition() - globalPos.x - 1, (float)mScrollBarY.windowPosition() - globalPos.y - 1);
	target.setView(view);

	Container::onDraw(target);

	// undo translation and clipping
	target.setView(orig_view);

	if (mCanScrollX) {
		target.draw(mScrollBarBackX);
		target.draw(mScrollBarGripX);
	}

	if (mCanScrollY) {
		target.draw(mScrollBarBackY);
		target.draw(mScrollBarGripY);
	}
}

bool ScrollContainer::onMouseDown(Vector2i pos, int button)
{
	if (button == 0)
	{
		if (mCanScrollX)
		{
			if (Box2i::sf2alo(mScrollBarGripX.getGlobalBounds()).contains(pos))
			{
				mLastMousePos = pos;
				mDraggingX = true;
				updateScrollBarColor();
				return true;
			}
		}

		if (mCanScrollY)
		{
			if (Box2i::sf2alo(mScrollBarGripY.getGlobalBounds()).contains(pos))
			{
				mLastMousePos = pos;
				mDraggingY = true;
				updateScrollBarColor();
				return true;
			}
		}
	}

	return false;
}

bool ScrollContainer::onMouseMoved(Vector2i pos)
{
	if (mDraggingX)
	{
		mScrollBarX.moveGripBy(pos.x - mLastMousePos.x);
		mLastMousePos = pos;
		updateScrollBarPos();
	}
	if (mDraggingY)
	{
		mScrollBarY.moveGripBy(pos.y - mLastMousePos.y);
		mLastMousePos = pos;
		updateScrollBarPos();
	}

	return false;
}

bool ScrollContainer::onMouseUp(Vector2i pos, int button)
{
	if (button == 0 && (mDraggingX || mDraggingY))
	{
		mDraggingY = false;
		mDraggingX = false;
		updateScrollBarColor();
		return true;
	}

	return false;
}

int ScrollContainer::scrollBarWidth() const
{
  return (int) ceil(12 * Desktop::get()->getDPIScale());
}

}

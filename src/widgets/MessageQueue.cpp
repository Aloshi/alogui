#include <alogui/widgets/MessageQueue.h>

#include <alogui/containers/ScrollContainer.h>
#include <assert.h>

namespace alogui {

MessageQueue::MessageQueue()
{
}

void MessageQueue::push(const std::string& msg, sf::Color color)
{
	bool skipLastChar = (msg.size() > 0 && msg[msg.size() - 1] == '\n');

	mEntries[mHead].widget.setText(skipLastChar ? msg.substr(0, msg.size() - 1) : msg);
	mEntries[mHead].widget.setTextColor(color);
	mEntries[mHead].widget.update(0.0f);

	mHead++;
	if (mHead == mTail) {  // full?
		mTail++;
	}

	requestResize();
}

void MessageQueue::clear()
{
	mTail = mHead;
	requestResize();
}

// TODO this should probably use heightForWidth
Vector2i MessageQueue::getSizeHint() const
{
	Vector2i hint(0, 0);
	for (auto it = mTail; it != mHead; it++) {
		const Widget* widget = &mEntries[it].widget;
		hint.y += widget->getSize().y;
		hint.x = std::max(hint.x, widget->getSize().x);
	}
	return hint;
}

void MessageQueue::onSizeChanged()
{
	int y = 0;
	for (auto it = mTail; it != mHead; it++) {
		Widget* widget = &mEntries[it].widget;
		widget->setPosition(0, y);
		widget->setSize(getSize().x, widget->useHeightForWidth() ? widget->heightForWidth(getSize().x) : widget->getSizeHint().y);
		y += widget->getSize().y;
	}
	
	int overage = y - getSize().y;
	assert(overage <= 0);
}

std::pair<MessageQueue::Iterator, MessageQueue::Iterator> MessageQueue::calcVisibleRange(int pxStartY, int pxEndY)
{
	float top = 0;
	Iterator startIt = mTail;
	while (startIt != mHead) {
		top += mEntries[*startIt].widget.getSize().y;
		if (top >= pxStartY)
			break;

		startIt++;
	}

	Iterator endIt = startIt;

	// I am sure there is an elegant and less stupid way to do this but it works
	if (endIt != mHead)
		endIt++;

	while (endIt != mHead) {
		if (top >= pxEndY)
			break;
		top += mEntries[*endIt].widget.getSize().y;

		endIt++;
	}

	return std::pair<Iterator, Iterator>(startIt, endIt);
}

void MessageQueue::onUpdate(float dt)
{
}

void MessageQueue::onDraw(sf::RenderTarget& target)
{
	// calculate range based on parent
	std::pair<Iterator, Iterator> visible(mTail, mHead);

	// TODO cache this dynamic_cast to onParentChanged or something?
	ScrollContainer* scroll = dynamic_cast<ScrollContainer*>(getParent());
	if (scroll != NULL) {
		const auto& sb = scroll->sbY();
		visible = calcVisibleRange(sb.windowPosition()-1, sb.windowPosition() + sb.windowSize()+1);  // TODO
	}

	for (auto it = visible.first; it != visible.second; it++) {
		mEntries[it].widget.draw(target);
	}
}

}  // namespace alogui

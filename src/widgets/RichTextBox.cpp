#include <alogui/widgets/RichTextBox.h>
#include <alogui/Desktop.h>  // for DPI scaling

#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/RenderTarget.hpp>

#include <SFML/Graphics/RectangleShape.hpp>

#include <sstream>

#include <assert.h>
#include <iostream>

namespace alogui
{

RichTextBox::RichTextBox()
{
	sf::Font f;
	bool ok = f.loadFromFile("font.ttf");
	assert(ok);
	mStyle.fonts.push_back(f);

	mStyle.colors.push_back(sf::Color::White);
	mStyle.colors.push_back(sf::Color::Red);
	mStyle.colors.push_back(sf::Color::Green);
	mStyle.colors.push_back(sf::Color::Blue);
}

void RichTextBox::setText(const std::string& text)
{
	mText = text;
	mChunks = parseChunks(text);

	rebuildDrawables();  // in case we don't get resized...
	requestResize();
}

Vector2i RichTextBox::getSizeHint() const
{
	Box2i bounds;
	createDrawables(std::numeric_limits<int>::max(), mText, mStyle, mChunks.begin(), mChunks.end(), &bounds);
	assert(bounds.top == 0);
	return bounds.size();
}

Vector2i RichTextBox::minimumSizeHint() const
{
	return Vector2i(12 * Desktop::get()->getDPIScale(), getSizeHint().y);
}

bool RichTextBox::useHeightForWidth() const
{
	return true;
}

int RichTextBox::heightForWidth(int w) const
{
	Box2i bounds;
	createDrawables(w, mText, mStyle, mChunks.begin(), mChunks.end(), &bounds);
	assert(bounds.top == 0);
	return bounds.height;
}

void RichTextBox::onSizeChanged()
{
	rebuildDrawables();
}

void RichTextBox::onDraw(sf::RenderTarget& target)
{
	for (unsigned int i = 0; i < mDrawables.size(); i++)
		target.draw(*mDrawables[i].get());
}

std::vector<RichTextBox::Chunk> RichTextBox::parseChunks(const std::string& str)
{
	std::vector<Chunk> out;

	Chunk nextChunk;
	nextChunk.start = 0;
	nextChunk.length = 0;
	nextChunk.color = 0;
	nextChunk.font = 0;
	nextChunk.charSize = 14 * Desktop::get()->getDPIScale();

	for (unsigned int i = 0; i < str.size(); i++)
	{
		if (str[i] == '\\')
		{
			// escaped slash?
			if (i + 1 < str.size() && str[i + 1] == '\\')
			{
				// skip second slash
				nextChunk.length = i - nextChunk.start;
				out.push_back(nextChunk);

				nextChunk.start = i + 1;

				i++;  // ignore second slash
				continue;
			}

			// color
			if (i + 2 < str.size() && str[i + 1] == 'c')
			{
				char color = str[i + 2] - '0';

				// start new chunk
				nextChunk.length = i - nextChunk.start;
				out.push_back(nextChunk);

				nextChunk.start = i + 3;
				nextChunk.color = color;

				i += 2;
			}
		}
	}

	nextChunk.length = str.size() - nextChunk.start;
	out.push_back(nextChunk);

	return out;
}

std::vector< std::unique_ptr<sf::Drawable> > RichTextBox::createDrawables(
	int maxWidth,
	const std::string& text,
	const RichTextStyle& style,
	const std::vector<Chunk>::const_iterator& begin,
	const std::vector<Chunk>::const_iterator& end,
	Box2i* bounds_out)
{
	std::vector< std::unique_ptr<sf::Drawable> > out;

	const int lineHeight = 18 * Desktop::get()->getDPIScale();  // TODO pull from font

	Vector2i pos(0, 0);  // where next drawable will go
	Box2i fullBounds(0, 0, 0, 0);  // bounding box of all generated drawables

	for (auto it = begin; it != end; it++)
	{
		const unsigned int end = (it->start + it->length);

		unsigned int from = it->start;
		unsigned int to = from;

		while (to < end)
		{
			auto drawable = std::make_unique<sf::Text>();
			drawable->setPosition(pos);
			drawable->setFillColor(style.colors.at(it->color));
			drawable->setFont(style.fonts.at(it->font));
			drawable->setCharacterSize(it->charSize);

			// calculate next to-end pair
			while (to < end)
			{
				size_t next_space_idx = text.find_first_of(" \t\n", to+1);

				// clamp to within this block
				if (next_space_idx > end || next_space_idx == std::string::npos)
					next_space_idx = end;

				std::string str = text.substr(from, next_space_idx - from);
				drawable->setString(str);
				sf::FloatRect bounds = drawable->getLocalBounds();
				bounds.left += pos.x;
				bounds.top += pos.y;

				// need to stop here
				// require from != to - always emit at least some characters to avoid infinite loop
				if ((static_cast<int>(bounds.left + bounds.width) > maxWidth) && (from != to || pos.x != 0) || text[to] == '\n')
				{
					// from-next_space_idx is too wide, so emit from-to
					break;
				} else {
					to = next_space_idx;
				}
			}

			std::string str = text.substr(from, to - from);
			drawable->setString(str);

			sf::FloatRect bounds = drawable->getLocalBounds();
			bounds.left += pos.x;
			bounds.top += pos.y;
			if (bounds.left + bounds.width > maxWidth || from == to || text[to] == '\n')
			{
				// end of line
				pos.y += lineHeight;
				pos.x = 0;

				// skip leading spaces on next 
				if (to < end && text[to] == ' ' || text[to] == '\t' || text[to] == '\n')
					to++;

			} else {
				pos.x += static_cast<int>(floor(bounds.width));
			}

			if (!str.empty())
			{
				out.push_back(std::move(drawable));

				// TODO change this to use line sizes for height so adding a "g" or "q" to the string doesn't magically change the sizing
				Box2i drawableBounds((int)floor(bounds.left), (int)floor(bounds.top), (int)ceil(bounds.width), (int)ceil(bounds.height));
				fullBounds = Box2i::merge(fullBounds, drawableBounds);
			}

			from = to;
		}
	}

	// write to bounds_out if caller is interested
	if (bounds_out)
		*bounds_out = fullBounds;

	return out;
}

void RichTextBox::rebuildDrawables()
{
	mDrawables = createDrawables(width(), mText, mStyle, mChunks.begin(), mChunks.end());
}

}

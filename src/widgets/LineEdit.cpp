#include <alogui/widgets/LineEdit.h>
#include <alogui/Desktop.h>  // for DPI scaling
#include <SFML/Graphics/RenderTarget.hpp>

#include <assert.h>
#include <iostream>

namespace alogui {

#define REPEAT_START_DELAY -0.22f
#define REPEAT_TIME 0.05f

inline size_t utf8_prevCursor(const std::string& str, size_t cursor)
{
	if (cursor == 0)
		return 0;

	do {
		cursor--;
	} while (cursor > 0 && (str[cursor] & 0xC0) == 0x80);
	return cursor;
}

inline size_t utf8_nextCursor(const std::string& str, size_t cursor)
{
	// compare to character at the cursor
	const char c = str[cursor];

	size_t result = cursor;
	if ((c & 0x80) == 0) // 0xxxxxxx, one byte character
	{
		result += 1;
	} else if ((c & 0xE0) == 0xC0) // 110xxxxx, two bytes left in character
	{
		result += 2;
	} else if ((c & 0xF0) == 0xE0) // 1110xxxx, three bytes left in character
	{
		result += 3;
	} else if ((c & 0xF8) == 0xF0) // 11110xxx, four bytes left in character
	{
		result += 4;
	} else
	{
		// error, invalid utf8 string
		// if this assert is tripped, the cursor is in the middle of a utf8 code point
		assert((c & 0xC0) != 0x80); // character is 10xxxxxx
		// if that wasn't it, something crazy happened
		assert(false); 
	}

	// don't go beyond str.end()
	return (result > str.length()) ? cursor : result;
}

bool is_textual_key(sf::Keyboard::Key key)
{
	typedef sf::Keyboard Kbd;
	if (Kbd::isKeyPressed(Kbd::LControl) || Kbd::isKeyPressed(Kbd::LControl)
		|| Kbd::isKeyPressed(Kbd::LAlt) || Kbd::isKeyPressed(Kbd::RAlt))
		return false;

	// A-Z, 0-9
	if (key >= sf::Keyboard::A && key <= sf::Keyboard::Num9)
		return true;

	// special keys on keyboard
	if (key >= sf::Keyboard::LBracket && key <= sf::Keyboard::Tab)
		return true;

	// numpad
	if (key >= sf::Keyboard::Add && key <= sf::Keyboard::Divide)
		return true;
	if (key >= sf::Keyboard::Numpad0 && key <= sf::Keyboard::Numpad9)
		return true;

	return false;
}

LineEdit::LineEdit()
	: mCursor(0), mRepeatAction(NONE), mBlinkTimer(0.0f)
{
	if (!mFont.loadFromFile("font.ttf"))
		assert(false);

	mTextDisplay.setCharacterSize(14 * Desktop::get()->getDPIScale());
	mTextDisplay.setFont(mFont);
	mTextDisplay.setFillColor(sf::Color(0, 0, 0));

	mBackground.setFillColor(sf::Color(233, 233, 233));
	mBackground.setCornerPointCount(3);
	mBackground.getCornerRadius(4);

	mCursorShape.setFillColor(sf::Color(66, 66, 66));

	// intentionally only promoting the result of the division to float
	mTextDisplay.setPosition((float) (getMargins().x / 2), (float) (getMargins().y / 2));

	setSizePolicy(SizePolicy(SizePolicy::Expanding, SizePolicy::Fixed));
}

Vector2i LineEdit::getSizeHint() const
{
	sf::FloatRect bounds = mTextDisplay.getLocalBounds();
	return Vector2i((int)floor(bounds.width) + getMargins().x, getFontHeight() + getMargins().y);
}

void LineEdit::onSizeChanged()
{
	mBackground.setSize(getSize());
	mCursorShape.setSize(Vector2f(getCursorWidth(), (float)(getSize().y - getMargins().y)));
	onCursorChanged();
}

void LineEdit::onDraw(sf::RenderTarget& target)
{
	target.draw(mBackground);

	// scissor so we only get what's inside this widget
	Vector2i globalPos = getGlobalPosition() + Vector2i((int)mBackground.getPosition().x, (int)mBackground.getPosition().y);
	Vector2i size = getSize() - Vector2i(width() - (int)mBackground.getSize().x, 0);
	sf::View view;
	view.reset(sf::FloatRect((float)globalPos.x, (float)globalPos.y, (float)size.x, (float)size.y));

	view.setViewport(sf::FloatRect(globalPos.x / (float)target.getSize().x,
		globalPos.y / (float)target.getSize().y,
		width() / (float)target.getSize().x,
		height() / (float)target.getSize().y));

	// move view based on mOffset
	view.move((sf::Vector2f) sf::Vector2i(mOffset) - (sf::Vector2f)globalPos - mBackground.getPosition());

	target.setView(view);
	target.draw(mTextDisplay);

	target.draw(mCursorShape);
}

bool LineEdit::onMouseDown(Vector2i pos, int button)
{
	return true;
}

bool LineEdit::onMouseUp(Vector2i pos, int button)
{
	return true;
}

bool LineEdit::onTextEntered(const std::string& text)
{
	typedef sf::Keyboard Kbd;
	if (Kbd::isKeyPressed(Kbd::LControl) || Kbd::isKeyPressed(Kbd::LControl)
		|| Kbd::isKeyPressed(Kbd::LAlt) || Kbd::isKeyPressed(Kbd::RAlt))
		return false;

	if (text.length() == 1 && text[0] == '\x8') {
		// backspace, remove last utf8 character
		deleteChar(-1);
	} else if (text.length() == 1 && text[0] == '\xD') {
		// newline
		onEnterPressed();
	} else {
		mText.insert(mCursor, text);
		mCursor += text.length();
		onTextChanged();
		onCursorChanged();
	}

	return true;
}

void LineEdit::moveCursor(int dir)
{
	if (dir > 0) {
		// move right
		mCursor = utf8_nextCursor(mText, mCursor);
	} else if (dir < 0) {
		// move left
		mCursor = utf8_prevCursor(mText, mCursor);
	}
	onCursorChanged();
}

void LineEdit::deleteChar(int dir) {
	if (dir > 0) {
		// delete next
		size_t cursor = utf8_nextCursor(mText, mCursor);
		mText.erase(mText.begin() + mCursor, mText.begin() + cursor);
		// don't move cursor
	} else {
		// delete prev
		size_t cursor = utf8_prevCursor(mText, mCursor);
		mText.erase(mText.begin() + cursor, mText.begin() + mCursor);
		mCursor = cursor;
		onCursorChanged();
	}

	onTextChanged();
}

void LineEdit::onTextChanged()
{
	mTextDisplay.setString(sf::String::fromUtf8(mText.begin(), mText.end()));
}

void LineEdit::onCursorChanged()
{
	mCursorShape.setPosition(mTextDisplay.findCharacterPos(mCursor) - sf::Vector2f(mCursorShape.getSize().x / 2.0f, 0));

	// also need to move our view to make sure cursor is within view
	sf::Vector2f pos = mCursorShape.getPosition();
	sf::Vector2f localPos = pos - (sf::Vector2f)mOffset;
	if (localPos.x < 0)
		mOffset.x = (int)roundf(pos.x) - getCursorWidth();
	if (localPos.x > mBackground.getSize().x)
		mOffset.x = (int)roundf(pos.x - mBackground.getSize().x) + getCursorWidth();

	mOffset.x = std::max(std::min(mOffset.x, (int)mTextDisplay.getLocalBounds().width), 0);
}

void LineEdit::onEnterPressed()
{
	if (mEnterPressedCallback)
		mEnterPressedCallback(this);
}

void LineEdit::onUpdate(float dt)
{
	if (mRepeatAction != NONE) {
		mRepeatTimer += dt;
		while (mRepeatTimer >= REPEAT_TIME) {
			mRepeatTimer -= REPEAT_TIME;

			switch (mRepeatAction) {
			case MOVE_LEFT:
				moveCursor(-1);
				break;
			case MOVE_RIGHT:
				moveCursor(+1);
				break;
			case DELETE_NEXT_CHAR:
				deleteChar(+1);
				break;
			}
		}
	}
}

bool LineEdit::onKeyDown(Event::KeyCode key)
{
	if (key == sf::Keyboard::Left || key == sf::Keyboard::Right) {
		mRepeatAction = (key == sf::Keyboard::Left ? MOVE_LEFT : MOVE_RIGHT);
		moveCursor(mRepeatAction);
		mRepeatTimer = REPEAT_START_DELAY;
		return true;
	}
	if (key == sf::Keyboard::Delete) {
		mRepeatAction = DELETE_NEXT_CHAR;
		deleteChar(+1);
		mRepeatTimer = REPEAT_START_DELAY;
		return true;
	}
	
	// take typing keys
	if (is_textual_key(key)) {
		return true;
	}

	return false;
}

bool LineEdit::onKeyUp(Event::KeyCode key)
{
	if (key == sf::Keyboard::Left || key == sf::Keyboard::Right) {
		mRepeatAction = NONE;
		return true;
	}

	// take typing keys
	if (is_textual_key(key)) {
		return true;
	}

	return false;
}

void LineEdit::setText(const std::string& str)
{
	mText = str;
	mCursor = mText.size();

	onTextChanged();
	onCursorChanged();
}

void LineEdit::setEnterPressedCallback(const std::function<void(LineEdit*)>& cb)
{
	mEnterPressedCallback = cb;
}


Vector2i LineEdit::getMargins() const
{
  return Vector2i((int) ceil(8 * Desktop::get()->getDPIScale()), 0);
}

float LineEdit::getFontHeight() const
{
  return mFont.getLineSpacing(mTextDisplay.getCharacterSize());
  // return 18 * Desktop::get()->getDPIScale();
}

float LineEdit::getCursorWidth() const
{
  return 2 * Desktop::get()->getDPIScale();
}

}

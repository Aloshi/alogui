#include <alogui/widgets/Gallery.h>
#include <alogui/containers/ScrollContainer.h>
#include <alogui/Desktop.h>  // for DPI scaling
#include <SFML/Graphics/RenderTarget.hpp>

namespace alogui
{

Gallery::Gallery()
	: mTileSize((int) (64 * Desktop::get()->getDPIScale())),
    mTileSpacing((int) (6 * Desktop::get()->getDPIScale())),
    mSelectedIdx(-1), mHighlightedIdx(-1)
{
}

Gallery::~Gallery()
{
	clear();
}

bool Gallery::useHeightForWidth() const
{
	return true;
}

int Gallery::heightForWidth(int width) const
{
	if (mEntries.empty())
		return 0;

	int ncols = std::max((width + mTileSpacing) / (mTileSize + mTileSpacing), 1);
	int nrows = mEntries.size() / ncols;
	if (mEntries.size() % ncols != 0)
		nrows += 1;

	return nrows * (mTileSize + mTileSpacing) - mTileSpacing;
}

Vector2i Gallery::getSizeHint() const
{
	if (mEntries.empty())
		return Vector2i(0, 0);

	int ncols = (int) sqrt(mEntries.size());
	int nrows = mEntries.size() / ncols;
	if (mEntries.size() % ncols != 0)
		nrows += 1;

	return Vector2i(ncols * (mTileSize + mTileSpacing) - mTileSpacing,
		nrows * (mTileSize + mTileSpacing) - mTileSpacing);
}

Vector2i Gallery::minimumSizeHint() const
{
	return Vector2i(mTileSize, mTileSize);
}

void Gallery::onSizeChanged()
{
	if (mEntries.empty()) {
		mDims = Vector2i(0, 0);
		return;
	}

	// update mDims
	{
		int ncols = std::max((getSize().x + mTileSpacing) / (mTileSize + mTileSpacing), 1);
		int nrows = mEntries.size() / ncols;
		if (mEntries.size() % ncols != 0)
			nrows += 1;

		mDims.x = ncols;
		mDims.y = nrows;
	}

	int sizeWithSpacing = mTileSize + mTileSpacing;
	for (unsigned int i = 0; i < mEntries.size(); i++) {
		int col = i % mDims.x;
		int row = i / mDims.x;

		const auto& e = mEntries[i].get();
		e->label.setPosition(sizeWithSpacing * col, sizeWithSpacing * row);
		e->label.setSize(mTileSize, mTileSize);
		e->label.setAlignment(alogui::Label::ALIGN_BOTTOMCENTER);

		sf::Vector2u texSize = e->texture->getSize();

		sf::Vector2i pos(sizeWithSpacing * col, sizeWithSpacing * row);
		sf::Vector2f scale(1, 1);
		sf::IntRect texRect((texSize.x - mTileSize) / 2, (texSize.y - mTileSize) / 2,
			mTileSize, mTileSize);

		if (texSize.x < (unsigned int)mTileSize) {
			texRect.left = 0;
			texRect.width = texSize.x;

			scale.x = (float)mTileSize / texSize.x;
			// pos.x += (mTileSize - texSize.x) / 2;  // center
		}
		if (texSize.y < (unsigned int)mTileSize) {
			texRect.top = 0;
			texRect.height = texSize.y;

			scale.y = (float)mTileSize / texSize.y;  // scale
			// pos.y += (mTileSize - texSize.y) / 2;  // center
		}

		e->background.setTextureRect(texRect);
		e->background.setPosition((float)pos.x, (float)pos.y);
		e->background.setScale(scale);
	}

	mHighlightedIdx = -1;
	updateBackground(mHighlightedBackground, mHighlightedIdx);
	updateBackground(mSelectedBackground, mSelectedIdx);
}

void Gallery::onDraw(sf::RenderTarget& target)
{
	if (mSelectedIdx != -1)
		target.draw(mSelectedBackground);
	if (mHighlightedIdx != -1)
		target.draw(mHighlightedBackground);

	Box2i visible(0, 0, mDims.x, mDims.y);

	// TODO cache this dynamic_cast to onParentChanged or something?
	/*ScrollContainer* scroll = dynamic_cast<ScrollContainer*>(getParent());
	if (scroll != NULL) {
		const auto& sb = scroll->sbY();
		visible = calcVisibleRange(sb.windowPosition() - 1, sb.windowPosition() + sb.windowSize() + 1);
	}*/

	for (int y = visible.top; y < visible.top + visible.height; y++) {
		for (int x = visible.left; x < visible.left + visible.width; x++) {
			unsigned int idx = y * mDims.x + x;

			if (idx >= mEntries.size())
				return;

			const auto& e = mEntries[idx].get();

			target.draw(e->background);
			e->label.draw(target);
		}
	}
}

void Gallery::add(const std::string& label, const sf::Texture* tex)
{
	mEntries.push_back(std::make_unique<Entry>(label, tex));
	onAdded(mEntries.size() - 1);
	requestResize();
}

void Gallery::remove(int idx)
{
	if (idx < 0 || idx >= (int) mEntries.size())
		return;

	mEntries.erase(mEntries.begin() + idx);
	onRemoved(idx);

	if (mSelectedIdx != -1 && mSelectedIdx >= idx) {
		if (mSelectedIdx == idx)
			mSelectedIdx = -1;
		else if (mSelectedIdx > idx)
			mSelectedIdx--;

		onSelected(mSelectedIdx);
	}

	mHighlightedIdx = -1;
	requestResize();
}

void Gallery::clear()
{
	for (int idx = mEntries.size() - 1; idx >= 0; idx--) {
		onRemoved(idx);
	}

	mEntries.clear();
	mHighlightedIdx = -1;
	mSelectedIdx = -1;
	onSelected(-1);
	requestResize();
}

void Gallery::setSelectedIdx(int idx)
{
	if (idx < 0 || idx >= (int) mEntries.size())
		idx = -1;

	if (idx == mSelectedIdx)
		return;

	mSelectedIdx = idx;
	updateBackground(mSelectedBackground, mSelectedIdx);
	onSelected(mSelectedIdx);
}

void Gallery::setTileSize(int size)
{
	mTileSize = size;
	requestResize();
}

void Gallery::setTileSpacing(int spacing)
{
	mTileSpacing = spacing;
	requestResize();
}

int Gallery::mouseToEntry(Vector2i mousePos) const
{
	int sizeAndSpacing = (mTileSize + mTileSpacing);
	int col = mousePos.x / sizeAndSpacing;
	int colRem = mousePos.x % sizeAndSpacing;

	int row = mousePos.y / sizeAndSpacing;
	int rowRem = mousePos.y % sizeAndSpacing;

	// inside spacing?
	if (rowRem > mTileSize || colRem > mTileSize || colRem < 0 || rowRem < 0)
		return -1;

	// outside?
	if (col < 0 || row < 0 || col >= mDims.x || row >= mDims.y)
		return -1;

	int idx = row * mDims.x + col;

	// outside or in the last row, but beyond our final entry?
	if (idx < 0 || idx >= (int) mEntries.size())
		return -1;

	return idx;
}

bool Gallery::onMouseDown(Vector2i pos, int button)
{
	if (button == 0) {
		setSelectedIdx(mouseToEntry(pos));
		return true;
	}

	return false;
}

bool Gallery::onMouseUp(Vector2i pos, int button)
{
	return false;
}

bool Gallery::onMouseMoved(Vector2i pos)
{
	mHighlightedIdx = mouseToEntry(pos);
	updateBackground(mHighlightedBackground, mHighlightedIdx);
	return false;
}

void Gallery::onFocusLost()
{
	mHighlightedIdx = -1;
	updateBackground(mHighlightedBackground, mHighlightedIdx);
}

void Gallery::updateBackground(sf::RectangleShape& bg, int idx)
{
	const int sizeWithSpacing = mTileSize + mTileSpacing;
	int col = idx % mDims.x;
	int row = idx / mDims.x;

	//const auto& e = mEntries.at(mSelectedIdx).get();
	bg.setPosition(sizeWithSpacing * col - mTileSpacing / 2.0f, sizeWithSpacing * row - mTileSpacing / 2.0f);
	bg.setSize(sf::Vector2f((float)sizeWithSpacing, (float)sizeWithSpacing));
}

Gallery::Entry::Entry(const std::string& text, const sf::Texture* tex)
{
	label.setText(text);
	texture = tex;

	if (tex != NULL)
		background.setTexture(*tex);
}

void Gallery::onSelected(int idx)
{
}

void Gallery::onAdded(int idx)
{
}

void Gallery::onRemoved(int idx)
{
}

}

#include <alogui/widgets/Label.h>
#include <alogui/Desktop.h>  // for DPI scaling

#include <SFML/Graphics/RenderTarget.hpp>

#include <assert.h>
#include <math.h>
#include <iostream>

#include <SFML/Graphics/RectangleShape.hpp>

namespace alogui
{

sf::Font createFont() {
	sf::Font font;
	bool ok = font.loadFromFile("font.ttf");
	assert(ok);
	return font;
}

Label::Label()
	: mAlignment(ALIGN_LEFT)
{
	static sf::Font font = createFont();
	mText.setFont(font);
	mText.setCharacterSize(14 * Desktop::get()->getDPIScale());
	mText.setFillColor(sf::Color::White);
	mText.setString("Label");

	setSizePolicy(SizePolicy(SizePolicy::Minimum, SizePolicy::Minimum));
}

void Label::setText(const std::string& text)
{
	mText.setString(text);
	requestResize();
}

void Label::setAlignment(Alignment align)
{
	mAlignment = align;
	repositionText();
}

Vector2i Label::getSizeHint() const
{
	const sf::FloatRect textRect = mText.getLocalBounds();
	return Vector2i(static_cast<int>(textRect.width), static_cast<int>(textRect.height)) + getPadding();
}

void Label::onSizeChanged()
{
	repositionText();
}

void Label::repositionText()
{
	// calculate the position for mText
	const sf::FloatRect textRect = mText.getLocalBounds();
	const Vector2i& padding = getPadding();
	float x = 0;
	float y = 0;

	// calculate x
	switch (mAlignment) {
	case ALIGN_TOPLEFT:
	case ALIGN_BOTTOMLEFT:
	case ALIGN_LEFT: x = (padding.x / 2.0f); break;

	case ALIGN_TOPCENTER:
	case ALIGN_BOTTOMCENTER:
	case ALIGN_MIDDLE: x = (width() - textRect.width) / 2.0f; break;

	case ALIGN_TOPRIGHT:
	case ALIGN_BOTTOMRIGHT:
	case ALIGN_RIGHT: x = (width() - textRect.width) - (padding.x / 2.0f); break;
	}

	// calculate y
	switch (mAlignment) {
	case ALIGN_TOPLEFT:
	case ALIGN_TOPCENTER:
	case ALIGN_TOPRIGHT: y = (padding.y / 2.0f); break;

	case ALIGN_LEFT:
	case ALIGN_MIDDLE:
	case ALIGN_RIGHT: y = (height() - textRect.height) / 2.0f; break;

	case ALIGN_BOTTOMLEFT:
	case ALIGN_BOTTOMCENTER:
	case ALIGN_BOTTOMRIGHT: y = (height() - textRect.height) - (padding.y / 2.0f); break;
	}

	mText.setPosition(floorf(x - textRect.left), floorf(y - textRect.top));
}

void Label::onDraw(sf::RenderTarget& target)
{
	/*static sf::RectangleShape rect;
	rect.setFillColor(sf::Color::Transparent);
	rect.setOutlineColor(sf::Color::Red);
	rect.setOutlineThickness(0.5f);
	rect.setPosition(0, 0);
	rect.setSize(getSize());
	target.draw(rect);*/

	target.draw(mText);
}

Vector2i Label::getPadding() const {
  return Vector2i(0, (int) ceil(6 * Desktop::get()->getDPIScale()));
}

}

#include <alogui/widgets/Button.h>

#include <SFML/Graphics/RenderTarget.hpp>

#include <iostream>

namespace alogui {

const Vector2i PADDING(12, 12);

Button::Button()
{
	mLabel.setText("Button");
	mLabel.setAlignment(Label::ALIGN_MIDDLE);

	mBackground.setOutlineThickness(1.0f);
	mBackground.setCornerRadius(2);
	mBackground.setCornerPointCount(3);
	setPressing(false);

	setSizePolicy(SizePolicy(SizePolicy::Minimum, SizePolicy::Minimum));
}

void Button::setText(const std::string& text)
{
	mLabel.setText(text);
	requestResize();
}

Vector2i Button::getSizeHint() const
{
	return mLabel.getSizeHint() + PADDING;
}

void Button::onSizeChanged()
{
	mBackground.setSize(getSize());
	mLabel.setSize(getSize());
}

void Button::onDraw(sf::RenderTarget& target)
{
	target.draw(mBackground);
	mLabel.draw(target);
}

bool Button::onMouseDown(Vector2i pos, int button)
{
	setPressing(true);
	return true;
}

bool Button::onMouseUp(Vector2i pos, int button)
{
	if (!mPressing)
		return false;

	setPressing(false);
	if (Box2i(Vector2i(0, 0), getSize()).contains(pos))
		onClick();

	return true;
}

void Button::setPressing(bool press)
{
	mPressing = press;
	if (press) {
		mBackground.setFillColor(sf::Color(64, 68, 74));
		mBackground.setOutlineColor(sf::Color(255, 255, 255));
	} else {
		mBackground.setFillColor(sf::Color(40, 44, 48));
		mBackground.setOutlineColor(sf::Color(240, 240, 240));
	}
}

void Button::onClick()
{

}

}
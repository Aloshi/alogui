I want a completely scriptable GUI.
Retained mode, for sure. Don't need an editor right now.
Support for gamepads would be nice.
Needs to integrate well with the running game.

#Memory Management
=================

I really really wish I could use shared_ptr with Duktape :(

#Containers
==========

Holds a group of widgets. Positions and sizes them according to some policy.

- When is arranging performed?

#Callbacks
=========

Base Widget class has

```cpp
virtual ~Widget() {
	dukglue_invalidate_reference(ctx, this);
}

template<typename... Args>
void triggerCallback(const char* callbackName, Args... args) {
	dukglue_call_method(ctx, this, callbackName, args...);
}
```

So button would work like this

```cpp
void Button::onClicked() {
	triggerCallback("onClick");
}
```

On the script side, we might do:
```cpp
var button = button || new Button();
button.onClicked = function() {
	print('Clicked button!');
};
```

- What if you need to register multiple callbacks?

Widgets
=======

- ~~Window~~
- ~~Label~~
- ~~Button~~
- Image
- ~~LineEdit~~
- ComboBox

Containers
==========

- ~~Vertical~~
- ~~Horizontal~~
- ~~Scroll~~
- Grid

#Input
=====

```cpp
bool handleEvent(const sf::Event&);
```

Focus
=====

Pretty necessary for gamepad control.
#pragma once

#include <vector>

namespace sf {
class Event;
class RenderTarget;
}

namespace alogui
{

class Widget;

class Desktop
{
public:
	static Desktop* get();

	void add(Widget* widget);
	void remove(Widget* widget);
	bool added(Widget* widget);

	bool handleEvent(const sf::Event& ev);
	void update(float dt);
	void draw(sf::RenderTarget& target);

  inline void setDPIScale(float scale) {
    mDPIScale = scale;
  }
  inline float getDPIScale() const {
    return mDPIScale;
  }

private:
	static Desktop* gInstance;

	Desktop();
	virtual ~Desktop();

	void makeActive(Widget* widget);
	std::vector<Widget*> mWidgets;
	float mDPIScale;
};

}

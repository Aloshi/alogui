#pragma once

#include <alogui/SingleContainer.h>
#include <alogui/drawables/RoundedRect.h>
#include <alogui/widgets/Label.h>

#include <SFML/Graphics/ConvexShape.hpp>
#include <SFML/Graphics/CircleShape.hpp>

namespace alogui
{

class Window : public SingleContainer
{
public:
	Window();

	virtual Vector2i getSizeHint() const override;
	virtual Vector2i minimumSizeHint() const override;
	virtual Vector2i maximumSizeHint() const override;

	void setTitle(const std::string& title);
	void setCanClose(bool close);

private:
	virtual void onSizeChanged() override;
	virtual void onUpdate(float dt) override;
	virtual void onDraw(sf::RenderTarget& target) override;

	virtual bool onMouseDown(Vector2i pos, int button) override;
	virtual bool onMouseMoved(Vector2i pos) override;
	virtual bool onMouseUp(Vector2i pos, int button) override;

	virtual void onClose();

	Vector2i titlebarSizeHint() const;
	Box2i titlebarBox() const;

	RoundedRectShape mBackground;

	RoundedRectShape mTitlebarBackground;
	Label mTitlebarLabel;
	sf::CircleShape mCloseButton;
	sf::ConvexShape mResizeHandle;

	bool mCanClose;

	// Window interaction
	bool mDragging;
	bool mResizing;
	bool mClosing;  // mousedown'd on close button
	Vector2i mLastMousePos;
	Vector2i mResizeDiff;
};

}
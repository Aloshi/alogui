#pragma once

#include <ostream>
#include <alogui/Vector2.h>

#include <SFML/Graphics/Rect.hpp>

namespace alogui {

template <typename T>
class Box2
{
public:
	T left;
	T top;

	T width;
	T height;

	inline Vector2<T> pos() const { return Vector2<T>(left, top); }
	inline Vector2<T> size() const { return Vector2<T>(width, height); }

	Box2() : left(0), top(0), width(0), height(0) {}

	Box2(const Vector2<T>& pos, const Vector2<T>& size) {
		left = pos.x;
		top = pos.y;

		width = size.x;
		height = size.y;
	}

	Box2(T _left, T _top, T _width, T _height) {
		this->left = _left;
		this->top = _top;

		this->width = _width;
		this->height = _height;
	}

	void setSize(const Vector2<T> size) {
		width = size.x;
		height = size.y;
	}

	bool operator==(const Box2<T>& rhs) const {
		return (left == rhs.left && top == rhs.top
			&& width == rhs.width && height == rhs.height);
	}

	bool contains(const Vector2<T> p) const {
		return (p.x >= left && p.y >= top && p.x <= (left + width) && p.y <= (top + height));
	}

	inline T right() const { return left + width; }
	inline T bottom() const { return top + height; }

	static Box2<T> sf2alo(const sf::FloatRect& r)
	{
		return Box2<T>((T)r.left, (T)r.top, (T)r.width, (T)r.height);
	}

	template <typename SfT>
	static Box2<T> sf2alo(const sf::Vector2<SfT>& p, const sf::Vector2<SfT>& s)
	{
		return Box2<T>(Vector2<T>((T)p.x, (T)p.y), Vector2<T>((T)s.x, (T)s.y));
	}

	// return a box containing both a and b
	static Box2<T> merge(const Box2<T>& a, const Box2<T>& b)
	{
		Vector2<T> min(std::min(a.left, b.left), std::min(a.top, b.top));
		Vector2<T> max(std::max(a.left + a.width, b.left + b.width),
			std::max(a.top + a.height, b.top + b.height));

		return Box2<T>(min.x, min.y, (max.x - min.x), (max.y - min.y));
	}
};

typedef Box2<int> Box2i;

template <typename T>
std::ostream& operator<<(std::ostream& stream, const Box2<T>& box)
{
	stream << box.left << ", " << box.top << " (" << box.width << "x" << box.height << ")";
	return stream;
}

}
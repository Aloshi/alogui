#pragma once

#include <alogui/Vector2.h>

namespace alogui {

class SizePolicy
{
public:
	enum PolicyFlag {
		GrowFlag = 1,
		ShrinkFlag = 2,
		ExpandFlag = 4
	};

	enum Policy {
		Fixed = 0,
		Minimum = GrowFlag,
		Preferred = ShrinkFlag | GrowFlag,
		Expanding = ShrinkFlag | GrowFlag | ExpandFlag,
	};

	SizePolicy(Policy x, Policy y) : mPolicy(x, y), mStretch(1, 1) {}

	inline int x() const { return mPolicy.x; }
	inline int y() const { return mPolicy.y; }
	inline Vector2i stretch() const { return mStretch; }

	inline void setStretch(int x, int y) {
		mStretch.x = x;
		mStretch.y = y;
	}

private:
	Vector2i mPolicy;
	Vector2i mStretch;
};

}
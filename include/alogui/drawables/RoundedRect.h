#pragma once

#include <SFML/Graphics/Shape.hpp>

namespace alogui
{

class RoundedRectShape : public sf::Shape
{
public:
	explicit RoundedRectShape(float radius = 4, unsigned int cornerPointCount = 4);

	void setSize(const sf::Vector2f& size);
	inline const sf::Vector2f& getSize() const { return mSize; }

	void setCornerRadius(float radius);
	void setCornerRadius(float topLeft, float topRight, float bottomRight, float bottomLeft);
	inline float getCornerRadius(int i) const { return (i >= 0 && i <= 4) ? mRadius[i] : 0; }

	void setCornerPointCount(unsigned int count);
	inline unsigned int getCornerPointCount() const { return mCornerPointCount; }

	std::size_t getPointCount() const override final;
	sf::Vector2f getPoint(std::size_t index) const override final;

private:
	sf::Vector2f mSize;
	float mRadius[4];
	unsigned int mCornerPointCount;
};

}
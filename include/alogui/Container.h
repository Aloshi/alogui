#pragma once

#include <alogui/Widget.h>

#include <vector>

namespace alogui
{

class Container : public Widget
{
public:
	virtual ~Container();

	void add(Widget* child);
	void remove(Widget* child);

	virtual bool handleEvent(const Event& ev) override;

	Widget* findChildAt(Vector2i pos);

protected:
	void removeChild(Widget* child) override { remove(child); }
	virtual void onUpdate(float dt) override;
	virtual void onDraw(sf::RenderTarget& target) override;

	// override these
	virtual bool onAdd(Widget* child) { return true; }
	virtual void onRemove(Widget* child) {}

	std::vector<Widget*> mChildren;
};

}
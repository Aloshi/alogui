#pragma once

#include <alogui/Vector2.h>

#include <SFML/Window/Keyboard.hpp>

namespace sf {
class Event;
}

namespace alogui {

struct Event
{
	typedef sf::Keyboard::Key KeyCode;

	struct {
		Vector2i pos;
		int button;
	} mouse;

	struct {
		KeyCode code;
	} key;

	struct {
		std::string text;
	} text;

	enum EventType {
		UNKNOWN,

		MOUSE_DOWN,
		MOUSE_UP,
		MOUSE_MOVED,

		KEY_DOWN,
		KEY_UP,

		TEXT_ENTERED
	} type;

	static Event fromSFMLEvent(const sf::Event& ev);
};

}
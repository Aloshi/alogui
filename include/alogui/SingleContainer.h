#pragma once

#include <alogui/Container.h>

namespace alogui
{

class SingleContainer : public Container
{
public:
	Widget* getChild() const;

protected:
	virtual bool onAdd(Widget* child) override;
};

}
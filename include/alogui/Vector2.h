#pragma once

#include <SFML/System/Vector2.hpp>

#include <type_traits>
#include <ostream>

namespace alogui
{

template <typename T>
class Vector2
{
public:
	T x;
	T y;

	Vector2() : x(0), y(0) {}
	Vector2(T x_, T y_) : x(x_), y(y_) {}

	Vector2<T>& operator+= (const Vector2<T>& rhs) {
		x += rhs.x;
		y += rhs.y;
		return *this;
	}
	Vector2<T>& operator-= (const Vector2<T>& rhs) {
		x -= rhs.x;
		y -= rhs.y;
		return *this;
	}

	Vector2<T> operator+ (const Vector2<T>& rhs) const { return Vector2<T>(x + rhs.x, y + rhs.y); }
	Vector2<T> operator- (const Vector2<T>& rhs) const { return Vector2<T>(x - rhs.x, y - rhs.y); }
	Vector2<T> operator* (T rhs) const { return Vector2<T>(x * rhs, y * rhs); }
	Vector2<T> operator/ (T rhs) const { return Vector2<T>(x / rhs, y / rhs); }
	bool operator==(const Vector2<T>& rhs) const { return (x == rhs.x && y == rhs.y); }
	bool operator!=(const Vector2<T>& rhs) const { return !(*this == rhs); }

	inline T operator[](int i) const {
		return (i == 0) ? x : y;
	}

	operator typename std::conditional<!std::is_same<T, float>::value, sf::Vector2f, void>::type () const { return sf::Vector2f(static_cast<float>(x), static_cast<float>(y)); }
	operator sf::Vector2<T>() const { return sf::Vector2<T>(x, y); }
};

template<typename T>
std::ostream& operator<<(std::ostream& stream, const Vector2<T>& vec) {
	stream << vec.x << ", " << vec.y;
	return stream;
}

typedef Vector2<int> Vector2i;
typedef Vector2<float> Vector2f;

}
#pragma once

#include <alogui/Vector2.h>
#include <alogui/Box2.h>
#include <alogui/Event.h>
#include <alogui/SizePolicy.h>

namespace sf {
class RenderTarget;
class Event;
}

namespace alogui
{

class Widget
{
public:
	Widget();
	virtual ~Widget();

	void update(float dt);
	void draw(sf::RenderTarget& target);

	virtual Vector2i getSizeHint() const = 0;  // override this
	virtual bool useHeightForWidth() const;
	virtual int heightForWidth(int width) const;
	virtual Vector2i maximumSizeHint() const;
	virtual Vector2i minimumSizeHint() const;
	Vector2i maximumSize() const;  // actual maximum (hint capped by size policy and mMaximumSize)
	Vector2i minimumSize() const;  // actual minimum (hint capped by size policy and mMinimumSize)

	void setMinimumSize(Vector2i size);
	inline void setMinimumSize(int w, int h) { setMinimumSize(Vector2i(w, h)); }

	void setMaximumSize(Vector2i size);
	inline void setMaximumSize(int w, int h) { setMaximumSize(Vector2i(w, h)); }

	inline void setFixedSize(Vector2i size) {
		setMinimumSize(size);
		setMaximumSize(size);
	}
	inline void setFixedSize(int width, int height) { setFixedSize(Vector2i(width, height)); }

	virtual void requestResize();

	// applies to the entire hierarchy
	Widget* getFocusWidget();
	void grabFocus();
	void clearFocus();

	virtual bool handleEvent(const Event& ev);

	void setPosition(Vector2i pos);
	inline void setPosition(int x, int y) { setPosition(Vector2i(x, y)); }
	inline const Vector2i& getPosition() const { return mPosition; }
	Vector2i getGlobalPosition() const;

	void setSize(Vector2i size);
	inline void setSize(int w, int h) { setSize(Vector2i(w, h)); }
	inline const Vector2i& getSize() const { return mSize; }

	inline Widget* getParent() { return mParent; }
	inline Box2i getBox() const { return Box2i(mPosition, mSize); }

	inline int left() const { return mPosition.x; }
	inline int top() const { return mPosition.y; }
	inline int right() const { return mPosition.x + mSize.x; }
	inline int bottom() const { return mPosition.y + mSize.y; }
	inline int width() const { return mSize.x; }
	inline int height() const { return mSize.y; }

	inline bool isTopLevel() const { return (mParent == NULL); }

	inline const SizePolicy& getSizePolicy() const { return mSizePolicy; }
	void setSizePolicy(const SizePolicy& sp);

	virtual void onWake() {}
	virtual void onSleep() {}

protected:
	virtual void removeChild(Widget* child) {}
	virtual void debugDraw(sf::RenderTarget& target);

	void onSizePolicyChanged();

	// override these
	virtual void onSizeChanged() {}
	virtual void onUpdate(float dt) {}
	virtual void onDraw(sf::RenderTarget& target) {}

	virtual bool onMouseDown(Vector2i pos, int button) { return false; }
	virtual bool onMouseUp(Vector2i pos, int button) { return false; }
	virtual bool onMouseMoved(Vector2i pos) { return false; }

	virtual bool onTextEntered(const std::string& str) { return false; }  // utf8 character
	virtual bool onKeyDown(Event::KeyCode key) { return false; }
	virtual bool onKeyUp(Event::KeyCode key) { return false; }

	virtual void onFocusLost() {}
	virtual void onFocusGained() {}

	friend class Container;
	Widget* mParent;

	Widget* getTopLevel();

	// user-specified min/max size (usually set outside of widget)
	Vector2i mMaximumSize;
	Vector2i mMinimumSize;

private:
	Vector2i mPosition;
	Vector2i mSize;
	SizePolicy mSizePolicy;

	bool mResizePending;
	Widget* mFocusWidget;
};

}
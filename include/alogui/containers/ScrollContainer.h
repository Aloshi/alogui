#pragma once

#include <alogui/SingleContainer.h>

#include <SFML/Graphics/RectangleShape.hpp>

#include <iostream>

namespace alogui {

// taken from http://csdgn.org/inform/scrollbar-mechanics
class ScrollBar
{
public:
	ScrollBar() : ScrollBar(0, 0, 0) {}

	ScrollBar(int windowSize, int contentSize, int trackSize);

	inline int trackSize() const { return mTrackSize; }
	inline int gripSize() const { return mGripSize; }
	inline int windowSize() const { return mWindowSize; }
	inline int contentSize() const { return mContentSize; }

	// this needs to be a float or we get rounding errors in moveGripBy()
	inline float gripPosition() const
	{
		if (mWindowSize == mContentSize)
			return 0;

		float windowPositionRatio = mWindowPosition / windowScrollAreaSize();
		return (trackScrollAreaSize() * windowPositionRatio);
	}

	inline void moveGripBy(int mousePositionDelta) {
		if (mWindowSize == mContentSize || mTrackSize == mGripSize)
			return;

		//Determine the new location of the grip
		float newGripPosition = gripPosition() + mousePositionDelta;

		//Limit the grip so that it is not flying off the track
		newGripPosition = std::min(std::max(newGripPosition, 0.0f), (float)trackScrollAreaSize());

		//Now we use the same algorithm we used in the windowPositionRatio, for the grip and track
		float newGripPositionRatio = static_cast<float>(newGripPosition) / trackScrollAreaSize();

		//and we apply it in the same way as we did to determine the grips location
		//but to the window instead
		mWindowPosition = newGripPositionRatio * windowScrollAreaSize();
	}

	inline int windowPosition() const { return static_cast<int>(mWindowPosition); }
	inline void setWindowPosition(int pos) {
		mWindowPosition = static_cast<float>(pos);

		// keep in bounds
		if (mWindowPosition + mWindowSize > mContentSize)
			mWindowPosition = static_cast<float>(mContentSize - mWindowSize);
		if (mWindowPosition < 0)
			mWindowPosition = 0.0f;
	}

private:
	inline int trackScrollAreaSize() const { return mTrackSize - mGripSize; }
	inline int windowScrollAreaSize() const { return mContentSize - mWindowSize; }

	int mWindowSize;
	int mContentSize;
	int mTrackSize;
	int mGripSize;

	float mWindowPosition;  // offset into content
};

class ScrollContainer : public SingleContainer
{
public:
	ScrollContainer();

	virtual Vector2i getSizeHint() const override;
	virtual bool useHeightForWidth() const override;
	virtual int heightForWidth(int width) const override;

	//void setPadding(int padding);
	inline int getPadding() const { return mPadding; }

	virtual void requestResize() override;

	inline const ScrollBar& sbX() const { return mScrollBarX; }
	inline const ScrollBar& sbY() const { return mScrollBarY; }

protected:
	virtual void onDraw(sf::RenderTarget& target) override;
	virtual void onSizeChanged() override;

	virtual bool onMouseDown(Vector2i pos, int button) override;
	virtual bool onMouseMoved(Vector2i pos) override;
	virtual bool onMouseUp(Vector2i pos, int button) override;

private:
	template <typename T>
	inline T clamp(T v, T min, T max)
	{
		if (v > max)
			v = max;
		if (v < min)
			v = min;
		return v;
	}

	int scrollBarWidth() const;
	inline sf::Color backColor() { return sf::Color(62, 62, 66); }
	inline sf::Color neutralColor() { return sf::Color(104, 104, 104); }
	// inline sf::Color highlightColor() { return sf::Color(158, 158, 158); }
	inline sf::Color draggingColor() { return sf::Color(239, 235, 239); }

	void updateScrollBarPos();
	void updateScrollBarBack();
	void updateScrollBarColor();

	int mPadding;
	bool mCanScrollX;
	bool mCanScrollY;

	Vector2i mLastMousePos;
	bool mDraggingX;
	bool mDraggingY;

	ScrollBar mScrollBarX;
	ScrollBar mScrollBarY;

	sf::RectangleShape mScrollBarGripX;
	sf::RectangleShape mScrollBarGripY;
	sf::RectangleShape mScrollBarBackX;
	sf::RectangleShape mScrollBarBackY;
};

}

#pragma once

#include <alogui/Container.h>

namespace alogui
{

// TODO cache min/max/preferred sizes with a vector of BoxItems

class BoxContainer : public Container
{
public:
	BoxContainer();

	virtual Vector2i getSizeHint() const override;
	virtual Vector2i minimumSizeHint() const override;
	virtual Vector2i maximumSizeHint() const override;

	enum Orientation : int {
		VERTICAL = 0,
		HORIZONTAL = 1
	};

	void setOrientation(Orientation orientation);
	inline Orientation getOrientation() const { return mOrientation; }

	void setPadding(int padding);
	inline int getPadding() const { return mPadding; }

protected:
	virtual void onSizeChanged() override;

private:
	typedef Vector2i (Widget::*size_method_t)() const;
	Vector2i computeSize(size_method_t method) const;

	struct BoxItem {
		int sizeHint;  // size hint on our orientation axis
		int maxSize;  // max size on our orientation axis
		int minSize;  // min size on our orientation axis
		bool expand;  // should expand along our orientation axis
		int stretch;  // stretch factor along our orientation axis
	};

	std::vector<BoxItem> calcItems() const;

	Orientation mOrientation;
	int mPadding;
};

}
#pragma once

#include <alogui/Widget.h>
#include <alogui/drawables/RoundedRect.h>

#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/Text.hpp>

#include <functional>

namespace alogui {

class LineEdit : public Widget
{
public:
	LineEdit();

	virtual Vector2i getSizeHint() const override;

	void setEnterPressedCallback(const std::function<void(LineEdit*)>& cb);

	inline const std::string& getText() const { return mText; }
	void setText(const std::string& str);

protected:
	virtual void onUpdate(float dt) override;
	virtual void onSizeChanged() override;
	virtual void onDraw(sf::RenderTarget& target) override;

	virtual bool onKeyDown(Event::KeyCode key) override;
	virtual bool onKeyUp(Event::KeyCode key) override;

	virtual bool onMouseDown(Vector2i pos, int button) override;
	virtual bool onMouseUp(Vector2i pos, int button) override;

	virtual bool onTextEntered(const std::string& text) override;

	virtual void onEnterPressed();

private:
	void onTextChanged();
	void onCursorChanged();

	void moveCursor(int dir);
	void deleteChar(int dir);

	Vector2i getMargins() const;
  float getFontHeight() const;
  float getCursorWidth() const;

	RoundedRectShape mBackground;
	sf::RectangleShape mCursorShape;
	sf::Font mFont;
	std::string mText;
	sf::Text mTextDisplay;

	Vector2i mOffset;
	size_t mCursor;

	enum Action : int {
		NONE = 0,
		MOVE_LEFT = -1,
		MOVE_RIGHT = +1,
		DELETE_NEXT_CHAR = 2
	};
	Action mRepeatAction;
	float mRepeatTimer;
	float mBlinkTimer;

	std::function<void(LineEdit*)> mEnterPressedCallback;
};

}

#pragma once

#include <alogui/Widget.h>
#include <alogui/widgets/Label.h>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <memory>

namespace alogui
{

class Gallery : public Widget {
public:
	Gallery();
	virtual ~Gallery();

	virtual Vector2i minimumSizeHint() const override;
	virtual Vector2i getSizeHint() const override;
	virtual bool useHeightForWidth() const override;
	virtual int heightForWidth(int width) const override;

	void add(const std::string& label, const sf::Texture* texture);
	void remove(int idx);
	void clear();

	void setSelectedIdx(int idx);
	void setTileSize(int size);
	void setTileSpacing(int spacing);

	inline Label* getLabel(int idx) {
		return &mEntries.at(idx)->label;
	}
	inline sf::Sprite* getSprite(int idx) {
		return &mEntries.at(idx)->background;
	}

protected:
	virtual void onDraw(sf::RenderTarget& target) override;
	virtual void onSizeChanged() override;

	virtual bool onMouseDown(Vector2i pos, int button) override;
	virtual bool onMouseUp(Vector2i pos, int button) override;
	virtual bool onMouseMoved(Vector2i pos) override;

	virtual void onFocusLost() override;
	//virtual void onFocusGained() override;

	virtual void onAdded(int idx);
	virtual void onRemoved(int idx);
	virtual void onSelected(int idx);

private:
	struct Entry {
		Label label;
		sf::Sprite background;
		const sf::Texture* texture;

		Entry() {}
		Entry(const std::string& text, const sf::Texture* tex);
	};

	sf::RectangleShape mSelectedBackground;
	sf::RectangleShape mHighlightedBackground;

	int mouseToEntry(Vector2i mousePos) const;
	void updateBackground(sf::RectangleShape& bg, int idx);

	int mSelectedIdx;
	int mHighlightedIdx;

	int mTileSize;
	int mTileSpacing;
	Vector2i mDims;
	std::vector< std::unique_ptr<Entry> > mEntries;
};

}
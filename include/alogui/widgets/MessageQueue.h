#pragma once

#include <utility>  // for pair
#include <string>

#include <alogui/Widget.h>
#include <alogui/widgets/Label.h>

namespace alogui {

class MessageQueue : public Widget {
public:
	MessageQueue();
	void push(const std::string& msg, sf::Color color = sf::Color::White);
	void clear();

protected:
	Vector2i getSizeHint() const override;
	void onSizeChanged() override;

	void onDraw(sf::RenderTarget& rt) override;
	void onUpdate(float dt) override;

private:
	static const int ENTRIES_LENGTH = 1024;

	struct Iterator {
		inline Iterator() : idx(0) {}

		int idx;

		inline Iterator& operator++(int) {
			idx = (idx + 1) % ENTRIES_LENGTH;
			return *this;
		}
		inline bool operator==(const Iterator& rhs) const {
			return rhs.idx == idx;
		}
		inline int operator*() const {
			return idx;
		}
		inline operator int() const {
			return idx;
		}
	};

	std::pair<Iterator, Iterator> calcVisibleRange(int pxStartY, int pxEndY);  // return value: start inclusive, end exclusive

	struct Entry {
		// std::string data;
		// float height;
		Label widget;
	};

	Entry mEntries[ENTRIES_LENGTH];
	Iterator mHead;
	Iterator mTail;
};

}  // namespace alogui
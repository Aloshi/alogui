#pragma once

#include <alogui/Widget.h>

#include <SFML/Graphics/Text.hpp>

namespace alogui
{

class Label : public Widget
{
public:
	Label();
	virtual Vector2i getSizeHint() const override;

	inline void setTextColor(const sf::Color& color) { mText.setFillColor(color); }
	inline const sf::Color& getTextColor() const { return mText.getFillColor(); }

	void setText(const std::string& text);
	inline std::string getText() const { return mText.getString(); }
	
	enum Alignment {
		ALIGN_TOPLEFT,
		ALIGN_TOPCENTER,
		ALIGN_TOPRIGHT,

		ALIGN_LEFT,
		ALIGN_MIDDLE,
		ALIGN_RIGHT,

		ALIGN_BOTTOMLEFT,
		ALIGN_BOTTOMCENTER,
		ALIGN_BOTTOMRIGHT
	};

	void setAlignment(Alignment align);
	inline Alignment getAlignment() const { return mAlignment; }

protected:
	virtual void onSizeChanged() override;
	virtual void onDraw(sf::RenderTarget& target) override;

private:
	Vector2i getPadding() const;
	void repositionText();

	sf::Text mText;
	Alignment mAlignment;
};

}

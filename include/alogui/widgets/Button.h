#include <alogui/Widget.h>

#include <alogui/widgets/Label.h>
#include <alogui/drawables/RoundedRect.h>

namespace alogui {

class Button : public Widget
{
public:
	Button();

	virtual Vector2i getSizeHint() const override;

	inline std::string getText() const { return mLabel.getText(); }
	void setText(const std::string& text);

protected:
	virtual void onSizeChanged() override;
	virtual void onDraw(sf::RenderTarget& target) override;

	virtual bool onMouseDown(Vector2i pos, int button) override;
	virtual bool onMouseUp(Vector2i pos, int button) override;

	virtual void onClick();

private:
	void setPressing(bool pressing);

	bool mPressing;
	RoundedRectShape mBackground;
	Label mLabel;
};

}
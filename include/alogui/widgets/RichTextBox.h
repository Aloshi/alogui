#pragma once

#include <alogui/Widget.h>

#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/Rect.hpp>

#include <memory>

namespace alogui
{

struct RichTextStyle
{
	std::vector<sf::Font> fonts;
	std::vector<sf::Color> colors;
};

class RichTextBox : public Widget
{
public:
	RichTextBox();

	virtual Vector2i getSizeHint() const override;
	virtual Vector2i minimumSizeHint() const override;
	virtual bool useHeightForWidth() const override;
	virtual int heightForWidth(int w) const override;

	inline const std::string& getText() const { return mText; }
	void setText(const std::string& text);

protected:
	virtual void onSizeChanged() override;
	virtual void onDraw(sf::RenderTarget& target) override;

private:
	struct Chunk {
		int color;
		int font;
		int charSize;
		unsigned int start;
		unsigned int length;
	};

	void rebuildDrawables();

	static std::vector<Chunk> parseChunks(const std::string& str);
	static std::vector< std::unique_ptr<sf::Drawable> > createDrawables(
		int maxWidth,
		const std::string& text,
		const RichTextStyle& style,
		const std::vector<Chunk>::const_iterator& begin,
		const std::vector<Chunk>::const_iterator& end,
		Box2i* bounds_out = NULL);

	std::string mText;
	RichTextStyle mStyle;

	std::vector<Chunk> mChunks;
	std::vector< std::unique_ptr<sf::Drawable> > mDrawables;
};

}
